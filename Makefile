
build:
	docker-compose build

run:
	docker-compose up

migrate:
	docker-compose exec django python manage.py migrate

migrations:
	docker-compose exec django python manage.py makemigrations

populate:
	docker-compose exec django python manage.py populate

psql:
	docker-compose exec postgres psql -U postgres

shell:
	docker-compose run --rm django python manage.py shell_plus

lint:
	docker-compose run --rm django flake8

test:
	docker-compose run --rm django pytest

type-checks:
	docker-compose run --rm django mypy whaleville

clean:
	find . -type f -name "*.pyc" -delete


ssh:
	ssh root@0.0.0.0

install-roles:
	rm -rf server/ansible/roles/external && ansible-galaxy install --roles-path server/ansible/roles/external -r server/ansible/roles/requirements.yml

provision:
	cd server/ansible/ && ansible-playbook provision.yml

deploy:
	cd server/ansible/ && ansible-playbook deploy.yml

debug:
	docker-compose run --service-ports django
