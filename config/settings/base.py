"""
Base settings to build other settings files upon.
"""

import environ
import logging.config


ROOT_DIR = environ.Path(__file__) - 3
APPS_DIR = ROOT_DIR.path('whaleville')

env = environ.Env()

READ_DOT_ENV_FILE = env.bool('DJANGO_READ_DOT_ENV_FILE', default=False)
if READ_DOT_ENV_FILE:
    # OS environment variables take precedence over variables from .env
    env.read_env(str(ROOT_DIR.path('.env')))

# GENERAL

DEBUG = env.bool('DJANGO_DEBUG', False)

TIME_ZONE = 'UTC'
LANGUAGE_CODE = 'uk-ua'
SITE_ID = 1
USE_I18N = True
USE_L10N = True
USE_TZ = True
USE_HTTPS = True

# DATABASES

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': env('POSTGRES_DB'),
        'USER': env('POSTGRES_USER'),
        'PASSWORD': env('POSTGRES_PASSWORD'),
        'HOST': env('POSTGRES_HOST'),
        'PORT': env('POSTGRES_PORT'),
        'ATOMIC_REQUESTS': True,
        'CONN_MAX_AGE': env.int('CONN_MAX_AGE', default=60)
    }
}

# URLS

ROOT_URLCONF = 'config.urls'
WSGI_APPLICATION = 'config.wsgi.application'

# APPS

DJANGO_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
]
THIRD_PARTY_APPS = [
    'corsheaders',
    'ordered_model',
    'graphene_django',
    # 'axes',
    'channels',
]
LOCAL_APPS = [
    'whaleville.users.apps.UsersAppConfig',
    'whaleville.core.apps.CoreAppConfig',
]

INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

# MIGRATIONS

MIGRATION_MODULES = {
    'sites': 'whaleville.contrib.sites.migrations'
}

# AUTHENTICATION

AUTHENTICATION_BACKENDS = [
    # 'axes.backends.AxesBackend',
    'graphql_jwt.backends.JSONWebTokenBackend',
    'django.contrib.auth.backends.ModelBackend',
]

AUTH_USER_MODEL = 'users.User'
LOGIN_URL = 'account_login'

# PASSWORDS

PASSWORD_HASHERS = [
    # https://docs.djangoproject.com/en/dev/topics/auth/passwords/#using-argon2-with-django
    'django.contrib.auth.hashers.Argon2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
    'django.contrib.auth.hashers.BCryptSHA256PasswordHasher',
    'django.contrib.auth.hashers.BCryptPasswordHasher',
]

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',  # noqa
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',  # noqa
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',  # noqa
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',  # noqa
    },
]

# MIDDLEWARE

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    # 'axes.middleware.AxesMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
]

# GRAPHENE

GRAPHENE = {
    'SCHEMA': 'config.schema.schema',
    'MIDDLEWARE': [
        'graphql_jwt.middleware.JSONWebTokenMiddleware',
    ],
}

# Channels
CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'channels.layers.InMemoryChannelLayer',
    },
}
ASGI_APPLICATION = "config.urls.application"

# STATIC

STATIC_ROOT = str(ROOT_DIR('staticfiles'))
STATIC_URL = '/static/'
STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
]

# MEDIA

MEDIA_ROOT = str(APPS_DIR('media'))
MEDIA_URL = '/media/'


# FIXTURES

FIXTURE_DIRS = (
    str(APPS_DIR.path('fixtures')),
)

# EMAIL

EMAIL_BACKEND = env('DJANGO_EMAIL_BACKEND',
                    default='django.core.mail.backends.smtp.EmailBackend')

# ADMIN

# Django Admin URL.
ADMIN_URL = 'admin/'
ADMINS = env.list('DJANGO_ADMINS', default=[
    ('Whaleville Tales', 'admin@whaleville.space'),
])
MANAGERS = ADMINS

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'OPTIONS': {
            'debug': DEBUG,
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
            ],
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]


# Celery

INSTALLED_APPS += ['whaleville.taskapp.celery.CeleryAppConfig']
if USE_TZ:
    CELERY_TIMEZONE = TIME_ZONE
CELERY_BROKER_URL = env('CELERY_BROKER_URL')
CELERY_RESULT_BACKEND = CELERY_BROKER_URL
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
# CELERYD_TASK_TIME_LIMIT = 5 * 60
# CELERYD_TASK_SOFT_TIME_LIMIT = 60
# BROKER_TRANSPORT_OPTIONS = {
#     'max_retries': 3, 'interval_start': 0,
#     'interval_step': 0.2, 'interval_max': 0.5
# }


FRONTEND_URL = env('FRONTEND_URL',
                   default='https://whaleville.space/')

# AXES_USERNAME_FORM_FIELD = 'login'
# AXES_FAILURE_LIMIT = 10
# AXES_COOLOFF_TIME = 2
# AXES_LOGGER = 'django'


CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_CREDENTIALS = True

# LOGGING
LOGGING_CONFIG = None
logging.config.dictConfig({
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'console': {
            # exact format is not important, this is the minimum information
            'format': '%(asctime)s %(name)s %(levelname)s %(message)s',
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'console',
        },
    },
    'loggers': {
        # root logger
        '': {
            'level': 'INFO',
            'handlers': ['console'],
        },
        'celery': {
            'handlers': ['console'],
            'level': 'INFO',
        },
    },
})

CHANNEL_LAYERS = {
    "default": {  # type: ignore
        "BACKEND": "channels_redis.core.RedisChannelLayer",
        "CONFIG": {
            "hosts": [('redis', '6379')],
        },
    },
}

ASGI_APPLICATION = 'config.urls.application'


# WHALEVILLE GENERAL
ACTION_DELAY = 7
HP_PERCENTS_TO_USE_POTIONS = 45
SKILL_USE_CHANCE = 60
SKILL_USE_EXHAUSTION_COST = 15
EXHAUSTION_ON_HIT_BASE = 15
RESTORATION_PERCENT_WHILE_RESTING = 5
RARE_ITEM_GENERATION_CHANCE = 40
MASTERPIECE_ITEM_GENERATION_CHANCE = 20
LEGENDARY_ITEM_GENERATION_CHANCE = 10

# WHALEVILLE FIGHT
HIT_OCCURS_CHANCE = 60
HIT_WIZARD_CHANCE = 45
HIT_ROGUE_CHANCE = 45
RESET_ARMOR_BOOST_ON_TURN_START = True

# WHALEVILLE GUILD
MIN_CHARACTERS_TO_GENERATE = 2
MAX_CHARACTERS_TO_GENERATE = 3

# WHALEVILLE ITEMS
MIN_MARKET_ITEMS_GENERATE = 6
MAX_MARKET_ITEMS_GENERATE = 16
MIN_CHARACTER_ITEMS_GENERATE = 3
MAX_CHARACTER_ITEMS_GENERATE = 6
MARKET_FEE = 12
LOOT_POTION_CHANCE = 55
LOOT_ENEMY_CHANCE = 40
MIN_INVENTORY_SLOTS = 6
MAX_INVENTORY_SLOTS = 15
