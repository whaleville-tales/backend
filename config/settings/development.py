import logging

from .base import *  # noqa
from .base import env, APPS_DIR

# GENERAL
# ------------------------------------------------------------------------------
DEBUG = True
SECRET_KEY = env('DJANGO_SECRET_KEY',
                 default='pguF28rga8rGC99i7ouincWQZg21shjmxRiywAVs8p5SvWBIn9peCupBkyx3ggZi')
USE_HTTPS = False

ALLOWED_HOSTS = [
    'localhost',
    '0.0.0.0',
    '127.0.0.1',
]

INSTALLED_APPS += [   # noqa F405
    'django_extensions',
    'nplusone.ext.django',
]

FRONTEND_URL = 'http://0.0.0.0:5000'

MIDDLEWARE = ['nplusone.ext.django.NPlusOneMiddleware', ] + MIDDLEWARE   # noqa F405

MIDDLEWARE += [
    # https://github.com/bradmontgomery/django-querycount
    # Prints how many queries were executed, useful for the APIs.
    'querycount.middleware.QueryCountMiddleware',
]  # noqa F405

# nplusone
# https://github.com/jmcarp/nplusone
NPLUSONE_LOGGER = logging.getLogger('django')
NPLUSONE_LOG_LEVEL = logging.WARN

# django-debug-toolbar
# ------------------------------------------------------------------------------
if env.bool('DJANGO_DEBUG_TOOLBAR', False):
    INSTALLED_APPS += [   # noqa F405
        'debug_toolbar',
    ]

    MIDDLEWARE += [
        'debug_toolbar.middleware.DebugToolbarMiddleware',
    ]  # noqa

    DEBUG_TOOLBAR_CONFIG = {
        'DISABLE_PANELS': [
            'debug_toolbar.panels.redirects.RedirectsPanel',
        ],
        'SHOW_TEMPLATE_CONTEXT': True,
    }

# CACHES
# ------------------------------------------------------------------------------
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    },
    'axes_cache': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}

AXES_CACHE = 'axes_cache'


# EMAIL
# ------------------------------------------------------------------------------
EMAIL_BACKEND = env('DJANGO_EMAIL_BACKEND',
                    default='django.core.mail.backends.console.EmailBackend')
EMAIL_HOST = 'localhost'
EMAIL_PORT = 1025

# https://django-debug-toolbar.readthedocs.io/en/latest/installation.html#internal-ips
INTERNAL_IPS = ['127.0.0.1', '10.0.2.2']
if env('USE_DOCKER') == 'yes':
    import socket
    hostname, _, ips = socket.gethostbyname_ex(socket.gethostname())
    INTERNAL_IPS += [ip[:-1] + '1' for ip in ips]

# Celery
# ------------------------------------------------------------------------------
# http://docs.celeryproject.org/en/latest/userguide/configuration.html#task-always-eager
CELERY_TASK_ALWAYS_EAGER = False
CELERY_TASK_EAGER_PROPAGATES = False


# WhiteNoise

MIDDLEWARE.insert(3, 'whitenoise.middleware.WhiteNoiseMiddleware')  # noqa F405