import graphene
import graphql_jwt
from graphql_jwt.decorators import login_required
from whaleville.core.resolvers.quest.schema import QuestQuery, QuestMutation
from whaleville.core.resolvers.game.schema import GameQuery, GameMutation
from whaleville.core.resolvers.event.schema import EventQuery, EventMutation
from whaleville.core.resolvers.action_log.schema import (
    ActionLogQuery, ActionLogMutation)
from whaleville.core.resolvers.action_log.subscription import NewLogSubscription  # noqa
from whaleville.core.resolvers.quest_chain.schema import QuestChainQuery
from whaleville.core.resolvers.inventory.schema import InventoryQuery
from whaleville.core.resolvers.item.schema import ItemMutation
from whaleville.core.resolvers.character.schema import (CharacterQuery,
                                                        CharacterMutation)
from whaleville.users.schema import UserMutation
from whaleville.core.resolvers.types import UserType


class Query(InventoryQuery, QuestChainQuery, ActionLogQuery,
            EventQuery, GameQuery, QuestQuery, CharacterQuery,
            graphene.ObjectType):
    user = graphene.Field(UserType)

    @login_required
    def resolve_user(self, info, **kwargs):
        user = info.context.user
        return user


class Mutation(ActionLogMutation, ItemMutation, EventMutation,
               GameMutation, UserMutation,
               QuestMutation, CharacterMutation, graphene.ObjectType):
    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()


class Subscription(graphene.ObjectType):
    """Root GraphQL subscription."""
    new_log_subscription = NewLogSubscription.Field()


schema = graphene.Schema(
    query=Query,
    mutation=Mutation,
)
