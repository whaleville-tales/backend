import time
from config.settings.base import (
    ACTION_DELAY, RESTORATION_PERCENT_WHILE_RESTING)
from whaleville.taskapp.celery import app
from whaleville.core.models.character.character import Character


@app.task(name='restore_party_characters')
def restore_party_characters(party_ids):
    for character_id in party_ids:
        character = Character.objects.get(uuid=character_id)
        current_hp = character.attributes.current_hp
        current_exhaustion = character.attributes.current_exhaustion
        total_hp = character.attributes.total_hp
        total_exhaustion = character.attributes.total_exhaustion
        character.attributes.armor_boost = 0
        character.attributes.save()

        while current_exhaustion != 0 or current_hp != total_hp:
            if current_hp != total_hp:
                current_hp += (total_hp / 100) * \
                    RESTORATION_PERCENT_WHILE_RESTING
                if current_hp > total_hp:
                    current_hp = total_hp
                character.attributes.current_hp = current_hp
            if current_exhaustion != total_exhaustion:
                current_exhaustion -= (total_exhaustion / 100) * RESTORATION_PERCENT_WHILE_RESTING  # noqa
                if current_exhaustion < 0:
                    current_exhaustion = 0
                character.attributes.current_exhaustion = current_exhaustion
            character.attributes.save()
            time.sleep(ACTION_DELAY / 3)
