import json
from django.contrib import admin
from django.core.serializers.json import DjangoJSONEncoder
from django.db.models import Count
from django.db.models.functions import TruncDay
from ordered_model.admin import OrderedModelAdmin

from whaleville.core.models.quest.quest import Quest
from whaleville.core.models.quest.quest_data import QuestData
from whaleville.core.models.quest.quest_chain import QuestChain
from whaleville.core.models.quest.challenge import Challenge
from whaleville.core.models.quest.challenge_data import ChallengeData
from whaleville.core.models.game import Game
from whaleville.core.models.event import Event
from whaleville.core.models.action_log import ActionLog

from whaleville.core.models.character.character import Character
from whaleville.core.models.character.attributes import Attributes
from whaleville.core.models.character.characteristics import Characteristics
from whaleville.core.models.character.death import Death
from whaleville.core.models.character.skill import Skill

from whaleville.core.models.inventory.item import Item
from whaleville.core.models.inventory.inventory import Inventory


class ChallengeInline(admin.StackedInline):
    model = Challenge
    extra = 0


class CharacterInline(admin.StackedInline):
    model = Character
    extra = 0


class ItemInline(admin.StackedInline):
    model = Item
    extra = 0


class SkillInline(admin.StackedInline):
    model = Skill
    extra = 0


class InventoryInline(admin.StackedInline):
    model = Inventory
    extra = 0


class InventoryAdmin(admin.ModelAdmin):
    inlines = [
        ItemInline,
    ]

    class Meta:
        model = Inventory


class AttributesInline(admin.TabularInline):
    model = Attributes
    extra = 0


class CharacteristicsInline(admin.TabularInline):
    model = Characteristics
    extra = 0


class CharacterAdmin(admin.ModelAdmin):
    inlines = [
        AttributesInline, CharacteristicsInline, SkillInline,
        InventoryInline, ItemInline
    ]

    class Meta:
        model = Character


class ChallengeDataAdmin(OrderedModelAdmin):
    list_display = ('description', 'move_up_down_links')

    class Meta:
        model = ChallengeData


class ChallengeAdmin(OrderedModelAdmin):
    inlines = [
        CharacterInline, ItemInline
    ]

    class Meta:
        model = Challenge


class ActionLogAdmin(admin.ModelAdmin):
    # display these table columns in the list view
    list_display = ("id", "text", "log_type", "quest", "created_at")
    ordering = ("-created_at",)

    def changelist_view(self, request, extra_context=None):
        # Aggregate new subscribers per day
        chart_data = (
            ActionLog.objects.annotate(date=TruncDay("created_at"))
            .values("date")
            .annotate(y=Count("id"))
            .order_by("-date")
        )

        # Serialize and attach the chart data to the template context
        as_json = json.dumps(list(chart_data), cls=DjangoJSONEncoder)
        extra_context = extra_context or {"chart_data": as_json}

        # Call the superclass changelist_view to render the page
        return super().changelist_view(request, extra_context=extra_context)


admin.site.register(Game)
admin.site.register(QuestData)
admin.site.register(Quest)
admin.site.register(QuestChain)
admin.site.register(ChallengeData, ChallengeDataAdmin)
admin.register(Challenge, ChallengeAdmin)
admin.site.register(Challenge)
admin.site.register(Event)
admin.site.register(ActionLog, ActionLogAdmin)

admin.site.register(Character, CharacterAdmin)
admin.site.register(Attributes)
admin.site.register(Characteristics)
admin.site.register(Death)
admin.site.register(Skill)

admin.site.register(Inventory, InventoryAdmin)
admin.site.register(Item)
