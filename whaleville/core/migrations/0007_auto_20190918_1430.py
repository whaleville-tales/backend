# Generated by Django 2.2 on 2019-09-18 14:30

from django.db import migrations, models
import whaleville.core.enums


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0006_challengedata_quest_title'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='game',
            name='finished',
        ),
        migrations.AlterField(
            model_name='quest',
            name='quest_type',
            field=models.CharField(choices=[('Active', whaleville.core.enums.QuestType('Active')), ('Completed', whaleville.core.enums.QuestType('Completed')), ('Closed', whaleville.core.enums.QuestType('Closed')), ('Failed', whaleville.core.enums.QuestType('Failed')), ('New', whaleville.core.enums.QuestType('New')), ('Hidden', whaleville.core.enums.QuestType('Hidden'))], default=whaleville.core.enums.QuestType('Hidden'), max_length=32),
        ),
    ]
