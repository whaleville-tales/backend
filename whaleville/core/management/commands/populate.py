from django.core.management.base import BaseCommand, CommandError
from whaleville.core.processors.generator import quest_generator
from whaleville.core.models.quest.quest_data import QuestData


class Command(BaseCommand):
    help = 'Populates db with static quests and challenges data'

    def handle(self, *args, **options):
        if len(QuestData.objects.all()) > 0:
            raise CommandError(
                'Database was already populated')
        else:
            try:
                quest_generator.create_quests_data()
            except Exception as e:
                raise CommandError(
                    'Error was triggered during database population: {0}'.format(str(e)))  # noqa

            self.stdout.write(self.style.SUCCESS(
                'Database was successfully populated'))
