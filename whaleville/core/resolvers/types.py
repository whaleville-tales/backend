from graphene import ObjectType, Int, Field, List, String, Boolean
from graphene_django import DjangoObjectType
from django.contrib.auth import get_user_model
from whaleville.core.models.quest.quest import Quest
from whaleville.core.models.quest.quest_chain import QuestChain
from whaleville.core.models.game import Game
from whaleville.core.models.event import Event
from whaleville.core.models.action_log import ActionLog
from whaleville.core.models.inventory.inventory import Inventory
from whaleville.core.models.inventory.item import Item
from whaleville.core.models.character.character import Character
from whaleville.core.models.character.attributes import Attributes
from whaleville.core.models.character.characteristics import Characteristics
from whaleville.core.models.character.death import Death
from whaleville.core.models.character.skill import Skill


class UserType(DjangoObjectType):

    class Meta:
        model = get_user_model()


class LevelType(ObjectType):
    current_level = Int()
    next_level_max_experience = Int()

    def resolve_current_level(self, info):
        return self.level

    def resolve_next_level_max_experience(self, info):
        return self.next


class QuestType(DjangoObjectType):
    reveal_quests = List(String)
    title = String()
    description = String()
    report = String()
    experience_value = String()
    reward_value = String()
    image_str = String()

    class Meta:
        model = Quest


class QuestChainType(DjangoObjectType):
    revealed_quests = List(QuestType)
    pending_quests = List(QuestType)

    class Meta:
        model = QuestChain


class EventType(DjangoObjectType):

    class Meta:
        model = Event


class ActionLogType(DjangoObjectType):

    class Meta:
        model = ActionLog


class InventoryType(DjangoObjectType):
    max_slots = Int()
    current_slots = Int()

    class Meta:
        model = Inventory


class ItemType(DjangoObjectType):
    market_price = Int()

    class Meta:
        model = Item


class GameType(DjangoObjectType):
    is_finished = Boolean()

    class Meta:
        model = Game


class AttributesType(DjangoObjectType):
    level = Field(LevelType)
    armor = Int()
    legs_armor = Field(ItemType)
    torso_armor = Field(ItemType)
    head_armor = Field(ItemType)
    shield_armor = Field(ItemType)
    weapon = Field(ItemType)
    quiver = Field(ItemType)
    damage = Int()
    total_hp = Int()
    total_exhaustion = Int()

    class Meta:
        model = Attributes


class SkillType(DjangoObjectType):

    class Meta:
        model = Skill


class CharacterType(DjangoObjectType):
    revealed_skills = List(SkillType)

    class Meta:
        model = Character


class CharacteristicsType(DjangoObjectType):
    blurred_strength = String()
    blurred_dexterity = String()
    blurred_constitution = String()
    blurred_intelligence = String()
    blurred_perception = String()

    class Meta:
        model = Characteristics


class DeathType(DjangoObjectType):
    days_together = Int()

    class Meta:
        model = Death
