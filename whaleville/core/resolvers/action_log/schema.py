from graphene import List, Int
from django.core.exceptions import ObjectDoesNotExist
from graphql_jwt.decorators import login_required
from whaleville.core.resolvers import types
from whaleville.core.models.action_log import ActionLog
from whaleville.core.enums import ActionLogTypeGraph, QuestType
from . import mutations


class ActionLogQuery(object):
    action_log = List(types.ActionLogType, skip=Int(), last=Int())
    action_logs = List(
        types.ActionLogType,
        action_log_type=ActionLogTypeGraph(), last=Int(), skip=Int())

    @login_required
    def resolve_action_log(self, info, last=None, skip=None):
        # FIXME
        # sometimes action log doesn't show latest events
        # it's because of backend not expected behavior on few
        # last action log records.
        # It starts to skip latest instead of skipping earlest records
        try:
            user = info.context.user
            active_quest = user.game.quests.get(
                quest_type=QuestType.ACTIVE.value)
            action_log = ActionLog.objects.filter(
                quest=active_quest).order_by('created_at')
            # pagination
            if skip:
                action_log = action_log[skip:]
            if last:
                action_log = action_log[:last]
            return action_log

        except ObjectDoesNotExist:
            try:
                user = info.context.user
                completed_quest = user.game.quests.get(
                    quest_type=QuestType.COMPLETED.value)
                action_log = list(
                    ActionLog.objects.filter(
                        quest=completed_quest))
                # pagination
                if skip:
                    action_log = action_log[:-skip]

                if last:
                    action_log = action_log[:last]
                return action_log
            except ObjectDoesNotExist:
                return []
            return []

    @login_required
    def resolve_action_logs(self, info, last=None, skip=None):
        try:
            logs = list(ActionLog.objects.all())
            # pagination
            if skip:
                logs = logs[skip:]

            if last:
                logs = logs[:last]
            return logs

        except ObjectDoesNotExist:
            return []


class ActionLogMutation(object):
    produce_fake_log = mutations.ProduceFakeLog.Field()
