import graphene
import channels_graphql_ws
from whaleville.core.models.action_log import ActionLog
from whaleville.core.resolvers.types import ActionLogType


class NewLogSubscription(channels_graphql_ws.Subscription):
    """Simple GraphQL subscription."""

    # Subscription payload.
    action_log = graphene.Field(ActionLogType)

    class Arguments:
        """Subscription arguments."""
        channel = graphene.String()

    @staticmethod
    def subscribe(root, info, channel=None):
        """Called when user subscribes."""
        # Return the list of subscription group names.
        return [channel] if channel is not None else None

    @staticmethod
    def publish(action_log_id, info, channel=None):
        """Called to notify the client."""
        action_log = ActionLog.objects.get(id=action_log_id)
        return NewLogSubscription(action_log=action_log)
