import graphene
from graphene.relay import ClientIDMutation

from whaleville.core.resolvers import types
from whaleville.core.models.action_log import ActionLog
from whaleville.core.models.quest.quest import Quest
from .subscription import NewLogSubscription


class ProduceFakeLog(ClientIDMutation):
    action_log = graphene.Field(types.ActionLogType)

    class Input:
        text = graphene.String()

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        user = info.context.user
        latest_quest = Quest.objects.filter(game=user.game).last()
        action_log = ActionLog(quest=latest_quest, text=input['text'])
        action_log.save()
        NewLogSubscription.broadcast(group=user.email, payload=action_log.id)
        return ProduceFakeLog(action_log=action_log)
