import graphene
from graphene.relay import ClientIDMutation
from django.core.exceptions import ObjectDoesNotExist
from graphql import GraphQLError

from whaleville.core.resolvers import types
from whaleville.core import enums
from whaleville.core.models.character.character import Character
from whaleville.core.models.character.death import Death
from whaleville.core.processors.generator import event_generator


# addexp
# decrease current_hp
# increase current_exhaustion
class ChangeCharacterPropsMutation(ClientIDMutation):
    character = graphene.Field(types.CharacterType)

    class Input:
        uuid = graphene.String()
        current_hp = graphene.Int()
        current_ex = graphene.Int()
        current_xp = graphene.Int()

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        required_fields = ('uuid')
        if set(required_fields).issubset(input):
            try:
                character = Character.objects.get(uuid=input['uuid'])
                if 'current_hp' in input:
                    character.attributes.current_hp = input['current_hp']
                if 'current_ex' in input:
                    character.attributes.current_exhaustion = input[
                        'current_ex']
                if 'current_xp' in input:
                    character.attributes.experience = input['current_xp']
                character.save()
                return ChangeCharacterPropsMutation(character=character)
            except ObjectDoesNotExist:
                raise GraphQLError('No character with provided uuid')
        else:
            raise GraphQLError('Validation error. These fields are requred: {0}'.format(  # noqa
                ', '.join(required_fields)))


# hire character
class HireCharacterMutation(ClientIDMutation):
    response = graphene.Boolean()

    class Input:
        uuid = graphene.String()

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        required_fields = ('uuid',)
        if set(required_fields).issubset(input):
            try:
                user = info.context.user
                character = Character.objects.get(uuid=input['uuid'])
                party_quantity = len(user.game.characters.filter(
                    character_type=enums.CharacterType.PARTY.value))
                shared_inventory = user.game.inventories.filter(
                    inventory_type=enums.InventoryType.SHARED.value)[0]
                if party_quantity > 2:
                    raise GraphQLError('Party is full')
                if shared_inventory.funds >= character.price:
                    character.character_type = enums.CharacterType.PARTY.value
                    shared_inventory.funds -= character.price
                    character.save()
                    shared_inventory.save()
                    event_generator.handle_new_member_event(
                        user.game, character)
                    return HireCharacterMutation(response=True)
                else:
                    raise GraphQLError('Not enough funds')
            except ObjectDoesNotExist:
                raise GraphQLError('No character with provided uuid')
        else:
            raise GraphQLError('Validation error. These fields are requred: {0}'.format(  # noqa
                ', '.join(required_fields)))


# proceed death
class ProceedCharacterDeathMutation(ClientIDMutation):
    character = graphene.Field(types.CharacterType)

    class Input:
        uuid = graphene.String()
        death_reason = graphene.String()

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        required_fields = ('uuid', 'death_reason')
        if set(required_fields).issubset(input):
            try:
                character = Character.objects.get(uuid=input['uuid'])
                character.status = enums.CharacterType.DEAD.value
                death = Death()
                death.character = character
                death.death_reason = input['death_reason']
                character.save()
                death.save()
                return ProceedCharacterDeathMutation(character=character)
            except ObjectDoesNotExist:
                raise GraphQLError('No character with provided uuid')
        else:
            raise GraphQLError('Validation error. These fields are requred: {0}'.format(  # noqa
                ', '.join(required_fields)))
