from graphene import Field, List, Boolean
from django.core.exceptions import ObjectDoesNotExist
from graphql_jwt.decorators import login_required
from whaleville.core.resolvers import types
from whaleville.core import enums
from whaleville.core.processors import restore_processor
from . import mutations


class CharacterQuery(object):
    character = Field(types.CharacterType)
    characters = List(
        types.CharacterType, character_type=enums.CharacterTypeGraph())
    restore_characters = Boolean()
    restore_party_characters = Boolean()

    @login_required
    def resolve_characters(
            self, info, character_type):
        character_type_flag = None
        if enums.CharacterType.GUILD.value == character_type:
            character_type_flag = enums.CharacterType.GUILD.value
        elif enums.CharacterType.PARTY.value == character_type:
            character_type_flag = enums.CharacterType.PARTY.value
        elif enums.CharacterType.DEAD.value == character_type:
            character_type_flag = enums.CharacterType.DEAD.value
        elif enums.CharacterType.NPC.value == character_type:
            character_type_flag = enums.CharacterType.NPC.value
        else:
            character_type_flag = enums.CharacterType.ENEMY.value
        try:
            user = info.context.user
            if character_type_flag == enums.CharacterType.DEAD.value:
                result = user.game.characters.filter(
                    character_type=character_type_flag).exclude(death=None)
            else:
                result = user.game.characters.filter(
                    character_type=character_type_flag)
            return result

        except ObjectDoesNotExist:
            return []

    @login_required
    def resolve_restore_party_characters(self, info):
        user = info.context.user
        party = user.game.characters.filter(
            character_type=enums.CharacterType.PARTY.value)
        for character in party:
            restore_processor.thread_restore_character_process(character)
        return True


class CharacterMutation(object):
    change_character_props = mutations.ChangeCharacterPropsMutation.Field()
    hire_character = mutations.HireCharacterMutation.Field()
    proceed_character_death = mutations.ProceedCharacterDeathMutation.Field()
