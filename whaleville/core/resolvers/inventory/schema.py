from graphene import Field, List, String
from django.core.exceptions import ObjectDoesNotExist
from graphql_jwt.decorators import login_required
from graphql import GraphQLError
from whaleville.core.models.character.character import Character
from whaleville.core.resolvers import types
from whaleville.core.enums import (
    InventoryTypeGraph, InventoryType, CharacterType)


class InventoryQuery(object):
    inventory = Field(
        types.InventoryType,
        inventory_type=InventoryTypeGraph(),
        character_uuid=String())
    market_inventories = List(
        types.InventoryType,
        inventory_type=InventoryTypeGraph())
    party_inventories = List(
        types.InventoryType, inventory_type=InventoryTypeGraph(),
    )

    @login_required
    def resolve_inventory(self, info, inventory_type, character_uuid=None):
        inventory_type_flag = None
        is_private = False
        if InventoryType.MARKET.value == inventory_type:
            inventory_type_flag = InventoryType.MARKET.value
        elif InventoryType.SHARED.value == inventory_type:
            inventory_type_flag = InventoryType.SHARED.value
        else:
            is_private = True
        try:
            user = info.context.user
            if is_private:
                if not character_uuid:
                    raise GraphQLError(
                        'Validation Error. character uuid is requred')
                character = Character.objects.get(
                    uuid=character_uuid)
                result = character.inventory.first()
            else:
                result = user.game.inventories.filter(
                    inventory_type=inventory_type_flag).first()
            return result

        except ObjectDoesNotExist:
            return []

    @login_required
    def resolve_market_inventories(self, info):
        try:
            user = info.context.user
            market = user.game.inventories.filter(
                inventory_type=InventoryType.MARKET.value).first()
            shared = user.game.inventories.filter(
                inventory_type=InventoryType.SHARED.value).first()
            return [market, shared]
        except ObjectDoesNotExist:
            return []

    @login_required
    def resolve_party_inventories(self, info):
        try:
            user = info.context.user
            private_inventories = user.game.inventories.filter(
                inventory_type=InventoryType.PRIVATE.value,
                owner__character_type=CharacterType.PARTY.value)
            shared = user.game.inventories.filter(
                inventory_type=InventoryType.SHARED.value).first()
            # return private_inventories.append(shared)
            inventories = [shared] + list(private_inventories)
            if len(inventories) < 4:
                inventories = inventories + [None]
            return inventories
        except ObjectDoesNotExist:
            return []
