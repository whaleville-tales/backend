import graphene
from graphene.relay import ClientIDMutation
from graphql import GraphQLError
from whaleville.core.enums import QuestTypeGraph, QuestType
# from whaleville.taskapp.tasks import process_quest
from whaleville.core.processors import quest_processor


class EditQuestTypeMutation(ClientIDMutation):
    response = graphene.Boolean()

    class Input:
        uuid = graphene.String()
        quest_type = QuestTypeGraph()

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        required_fields = ('uuid', 'quest_type')
        if set(required_fields).issubset(input):
            user = info.context.user
            quest = user.game.quests.get(
                uuid=input['uuid'])

            quest_type_flag = None
            if QuestType.ACTIVE.value == input['quest_type']:
                quest_type_flag = QuestType.ACTIVE.value
            elif QuestType.COMPLETED.value == input['quest_type']:
                quest_type_flag = QuestType.COMPLETED.value
            elif QuestType.FAILED.value == input['quest_type']:
                quest_type_flag = QuestType.FAILED.value
            elif QuestType.CLOSED.value == input['quest_type']:
                quest_type_flag = QuestType.CLOSED.value
            else:
                quest_type_flag = QuestType.NEW.value

            quest.quest_type = quest_type_flag
            quest.save()
            return EditQuestTypeMutation(response=True)

        else:
            raise GraphQLError('Validation error. These fields are requred: {0}'.format(  # noqa
                ', '.join(required_fields)))


class StartQuestMutation(ClientIDMutation):
    response = graphene.Boolean()

    class Input:
        uuid = graphene.String()
        restart = graphene.Boolean()

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        required_fields = ('uuid',)
        if set(required_fields).issubset(input):
            user = info.context.user
            quest = user.game.quests.get(
                uuid=input['uuid'])
            if (input['restart']):
                logs = quest.action_log.all()
                logs.delete()
            quest.quest_type = QuestType.ACTIVE.value
            quest.save()
            # initiate quest (async celery worker)
            # process_quest.delay(quest.uuid, user.game.pk)
            quest_processor.thread_quest_process(quest.uuid, user.game.pk)
            return StartQuestMutation(response=True)

        else:
            raise GraphQLError('Validation error. These fields are requred: {0}'.format(  # noqa
                ', '.join(required_fields)))


class FindQuestMutation(ClientIDMutation):
    response = graphene.Boolean()

    class Input:
        uuid = graphene.String()

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        required_fields = ('uuid',)
        if set(required_fields).issubset(input):
            user = info.context.user
            quest = user.game.quests.get(
                uuid=input['uuid'])
            quest.quest_type = QuestType.NEW.value
            quest.save()
            return FindQuestMutation(response=True)

        else:
            raise GraphQLError('Validation error. These fields are requred: {0}'.format(  # noqa
                ', '.join(required_fields)))


class CloseQuestMutation(ClientIDMutation):
    response = graphene.Boolean()

    class Input:
        uuid = graphene.String()

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        required_fields = ('uuid',)
        if set(required_fields).issubset(input):
            user = info.context.user
            quest = user.game.quests.get(
                uuid=input['uuid'])
            quest.quest_type = QuestType.CLOSED.value
            quest.save()
            return StartQuestMutation(response=True)

        else:
            raise GraphQLError('Validation error. These fields are requred: {0}'.format(  # noqa
                ', '.join(required_fields)))
