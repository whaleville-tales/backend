from graphene import Field, List, Int
from django.core.exceptions import ObjectDoesNotExist
from graphql_jwt.decorators import login_required
from whaleville.core.resolvers import types
from whaleville.core.enums import QuestTypeGraph, QuestType
from . import mutations


class QuestQuery(object):
    current_quest = Field(types.QuestType)
    latest_completed_quest = Field(types.QuestType)
    latest_failed_quest = Field(types.QuestType)
    quests = List(
        types.QuestType, quest_type=QuestTypeGraph(), first=Int(), skip=Int())
    search_pending_quests = List(types.QuestType)
    search_avaliable_quests = List(types.QuestType)

    @login_required
    def resolve_current_quest(self, info):
        try:
            user = info.context.user
            result = user.game.quests.get(
                quest_type=QuestType.ACTIVE.value)
            # pagination
            return result

        except ObjectDoesNotExist:
            return None

    @login_required
    def resolve_latest_completed_quest(self, info):
        try:
            user = info.context.user
            result = user.game.quests.get(
                quest_type=QuestType.COMPLETED.value)
            return result

        except ObjectDoesNotExist:
            return None

    @login_required
    def resolve_latest_failed_quest(self, info):
        try:
            user = info.context.user
            result = user.game.quests.get(
                quest_type=QuestType.FAILED.value)
            return result

        except ObjectDoesNotExist:
            return None

    @login_required
    def resolve_quests(self, info, quest_type, first=None, skip=None):
        quest_type_flag = None
        if QuestType.ACTIVE.value == quest_type:
            quest_type_flag = QuestType.ACTIVE.value
        elif QuestType.COMPLETED.value == quest_type:
            quest_type_flag = QuestType.COMPLETED.value
        elif QuestType.FAILED.value == quest_type:
            quest_type_flag = QuestType.FAILED.value
        else:
            quest_type_flag = QuestType.NEW.value
        try:
            user = info.context.user
            result = user.game.quests.filter(
                quest_type=quest_type_flag)
            # pagination
            if skip:
                result = result[skip:]

            if first:
                result = result[:first]
            return result

        except ObjectDoesNotExist:
            return []

    @login_required
    def resolve_search_pending_quests(self, info):
        try:
            pending_quests = []  # type: ignore
            user = info.context.user
            chains = user.game.quest_chains.all()
            for chain in chains:
                if chain.pending_quests:
                    pending_quests.extend(
                        chain.pending_quests)
            return list(dict.fromkeys(pending_quests))

        except ObjectDoesNotExist:
            return []
        pass

    @login_required
    def resolve_search_avaliable_quests(self, info):
        try:
            user = info.context.user
            avaliable_quests = user.game.quests.exclude(
                quest_type=QuestType.HIDDEN.value).exclude(
                quest_type=QuestType.CLOSED.value).exclude(
                quest_type=QuestType.COMPLETED.value)
            return avaliable_quests

        except ObjectDoesNotExist:
            return []


class QuestMutation(object):
    editQuestType = mutations.EditQuestTypeMutation.Field()
    startQuest = mutations.StartQuestMutation.Field()
    closeQuest = mutations.CloseQuestMutation.Field()
    findQuest = mutations.FindQuestMutation.Field()
