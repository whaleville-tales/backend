import graphene
from graphene.relay import ClientIDMutation
from graphql import GraphQLError

from whaleville.core.enums import EventTypeGraph, EventType
from whaleville.core.resolvers import types


class EventMutation(ClientIDMutation):
    event = graphene.Field(types.EventType)

    class Input:
        uuid = graphene.String()
        event_type = EventTypeGraph()

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        required_fields = ('uuid', 'event_type')
        if set(required_fields).issubset(input):
            user = info.context.user
            event = user.game.events.get(
                uuid=input['uuid'])

            event_type_flag = None
            if EventType.QUESTCOMPLETE.value == input['event_type']:
                event_type_flag = EventType.QUESTCOMPLETE.value
            elif EventType.LEVELUP.value == input['event_type']:
                event_type_flag = EventType.LEVELUP.value
            elif EventType.DEATH.value == input['event_type']:
                event_type_flag = EventType.DEATH.value
            else:
                event_type_flag = EventType.NEWMEMBER.value

            event.event_type = event_type_flag
            event.save()
            return EventMutation(event=event)

        else:
            raise GraphQLError('Validation error. These fields are requred: {0}'.format(  # noqa
                ', '.join(required_fields)))
