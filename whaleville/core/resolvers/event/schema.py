from graphene import Field, List, Int
from django.core.exceptions import ObjectDoesNotExist
from graphql_jwt.decorators import login_required
from whaleville.core.resolvers import types
from whaleville.core.enums import EventTypeGraph, EventType
from . import mutations


class EventQuery(object):
    latest_event = Field(types.EventType)
    events = List(
        types.EventType, event_type=EventTypeGraph(), first=Int(), skip=Int())
    events_all = List(types.EventType)

    @login_required
    def resolve_events(self, info, event_type, first=None, skip=None):
        event_type_flag = None
        if EventType.QUESTCOMPLETE.value == event_type:
            event_type_flag = EventType.QUESTCOMPLETE.value
        elif EventType.LEVELUP.value == event_type:
            event_type_flag = EventType.LEVELUP.value
        elif EventType.DEATH.value == event_type:
            event_type_flag = EventType.DEATH.value
        else:
            event_type_flag = EventType.NEWMEMBER.value
        try:
            user = info.context.user
            result = user.game.events_chain.filter(
                event_type=event_type_flag).order_by(
                '-created_at')
            # pagination
            if skip:
                result = result[skip:]

            if first:
                result = result[:first]
            return result

        except ObjectDoesNotExist:
            return []

    @login_required
    def resolve_events_all(self, info):
        try:
            user = info.context.user
            return user.game.events_chain.all().order_by(
                '-created_at')

        except ObjectDoesNotExist:
            return []

    @login_required
    def resolve_latest_event(self, info):
        try:
            user = info.context.user
            return user.game.events_chain.filter(
                event_type=EventType.QUESTCOMPLETE.value).last()

        except ObjectDoesNotExist:
            return None


class EventMutation(object):
    event = mutations.EventMutation.Field()
