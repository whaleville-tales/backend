import graphene
from graphene.relay import ClientIDMutation
from graphql import GraphQLError
from graphql_jwt.decorators import login_required
from whaleville.core import enums
from whaleville.core.models.inventory.item import Item
from whaleville.core.models.inventory.inventory import Inventory


class TransferItemMutation(ClientIDMutation):
    response = graphene.Boolean()

    class Input:
        uuid = graphene.String()
        destination_inventory_uuid = graphene.String()
        order = graphene.Int()

    @classmethod
    @login_required
    def mutate_and_get_payload(cls, root, info, **input):
        required_fields = ('uuid', 'destination_inventory_uuid', 'order')
        response = False
        if set(required_fields).issubset(input):
            item = Item.objects.get(uuid=input['uuid'])
            destination_inventory = Inventory.objects.get(
                uuid=input['destination_inventory_uuid'])
            # buy on market
            if item.inventory.inventory_type == enums.InventoryType.MARKET.value:  # noqa
                if destination_inventory.funds >= item.market_price:
                    item.inventory = destination_inventory
                    item.to(input['order'])
                    destination_inventory.funds -= item.market_price
                    item.save()
                    destination_inventory.save()
                    response = True
                else:
                    raise GraphQLError('Not enough funds')
            # sell on market
            elif destination_inventory.inventory_type == enums.InventoryType.MARKET.value:  # noqa
                shared_inventory = item.inventory
                shared_inventory.funds += item.price
                item.inventory = destination_inventory
                item.to(input['order'])
                item.save()
                destination_inventory.save()
                shared_inventory.save()
                response = True
            # share between shared-private-private Inventories
            else:
                item.inventory = destination_inventory
                item.to(input['order'])
                item.save()
                response = True
            return TransferItemMutation(response=response)

        else:
            raise GraphQLError('Validation error. These fields are requred: {0}'.format(  # noqa
                ', '.join(required_fields)))


class ReorderItemMutation(ClientIDMutation):
    response = graphene.Boolean()

    class Input:
        uuid = graphene.String()
        order = graphene.Int()

    @classmethod
    @login_required
    def mutate_and_get_payload(cls, root, info, **input):
        required_fields = ('uuid', 'order')
        if set(required_fields).issubset(input):
            item = Item.objects.get(uuid=input['uuid'])
            item.to(input['order'])
            return ReorderItemMutation(response=True)

        else:
            raise GraphQLError('Validation error. These fields are requred: {0}'.format(  # noqa
                ', '.join(required_fields)))
