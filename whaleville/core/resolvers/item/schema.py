from . import mutations


class ItemMutation(object):
    reorderItem = mutations.ReorderItemMutation.Field()
    transferItem = mutations.TransferItemMutation.Field()
