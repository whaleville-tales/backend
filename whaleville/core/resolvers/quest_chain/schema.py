from graphene import Field, List, Int
from django.core.exceptions import ObjectDoesNotExist
from graphql_jwt.decorators import login_required
from whaleville.core.models.quest.quest_chain import QuestChain
from whaleville.core.resolvers import types


class QuestChainQuery(object):
    quest_chain = Field(types.QuestChainType, id=Int())
    quest_chains = List(types.QuestChainType)

    @login_required
    def resolve_quest_chain(self, info, id=Int()):
        try:
            return QuestChain.objects.get(pk=id)
        except ObjectDoesNotExist:
            return []

    @login_required
    def resolve_quest_chains(self, info):
        try:
            user = info.context.user
            return user.game.quest_chains.all()

        except ObjectDoesNotExist:
            return []
