import graphene
from graphene.relay import ClientIDMutation
from graphql_jwt.decorators import login_required
from whaleville.core.processors.generator import game_starter


class ResetGameMutation(ClientIDMutation):
    response = graphene.Boolean()

    class Input:
        reset_game = graphene.Boolean()

    @classmethod
    @login_required
    def mutate_and_get_payload(cls, root, info, **input):
        if 'reset_game' in input:
            game_starter.reset_game(info.context.user)

        return ResetGameMutation(response=True)
