from graphene import Field, Boolean, String
from graphql_jwt.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from whaleville.core.resolvers import types
from whaleville.core.enums import QuestType
from whaleville.core.models.quest.quest_data import QuestData
from . import mutations


class GameQuery(object):
    game = Field(types.GameType)
    is_adventure_running = Boolean()
    is_game_finished = Boolean()
    get_current_background = String()

    @login_required
    def resolve_game(self, info):
        user = info.context.user
        return user.game

    @login_required
    def resolve_is_adventure_running(self, info):
        user = info.context.user
        try:
            user.game.quests.get(quest_type=QuestType.ACTIVE.value)
            return True
        except ObjectDoesNotExist:
            try:
                user.game.quests.get(
                    quest_type=QuestType.COMPLETED.value)
                return True
            except ObjectDoesNotExist:
                return False
            return False
        return False

    @login_required
    def resolve_is_game_finished(self, info):
        user = info.context.user
        closed_quests = user.game.quests.filter(
            quest_type=QuestType.CLOSED.value)
        quest_data_items = QuestData.objects.all()
        if len(closed_quests) == len(quest_data_items):
            return True
        return False

    @login_required
    def resolve_get_current_background(self, info):
        user = info.context.user
        try:
            active_quest = user.game.quests.get(
                quest_type=QuestType.ACTIVE.value)
            return active_quest.image_str
        except ObjectDoesNotExist:
            return 'CoastalTown'


class GameMutation(object):
    reset_game = mutations.ResetGameMutation.Field()
