from django.db import models

from whaleville.core.enums import EventType
from whaleville.core.models.quest.quest import Quest
from whaleville.core.models.game import Game
from whaleville.core.models.character.character import Character
from whaleville.core.models.base_models import TimeStampedModel, UUIDModel


class Event(TimeStampedModel, UUIDModel):
    description = models.TextField()
    event_type = models.CharField(
        max_length=32,
        choices=[
            (e_type.value, e_type) for e_type in EventType],
        default=EventType.QUESTCOMPLETE)
    image_str = models.CharField(max_length=200, blank=True)
    quest = models.OneToOneField(
        Quest, on_delete=models.CASCADE,
        null=True, blank=True)
    character = models.ForeignKey(
        Character, on_delete=models.CASCADE,
        null=True, blank=True)
    game = models.ForeignKey(
        Game, on_delete=models.CASCADE,
        related_name='events_chain', blank=True, null=True)

    def __str__(self):
        return '{0}... in {1}'.format(self.description[:30], self.game)
