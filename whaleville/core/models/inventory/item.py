from django.db import models
from ordered_model.models import OrderedModel
from config.settings.base import MARKET_FEE
from whaleville.core.models.inventory.inventory import Inventory
from whaleville.core.models.character.character import Character
from whaleville.core.models.quest.challenge import Challenge
from whaleville.core.enums import ItemType, ItemSpecial, ItemRarity
from whaleville.core.models.base_models import UUIDModel


class Item(OrderedModel, UUIDModel):
    price = models.IntegerField(default=0)
    quantity = models.IntegerField(default=1)
    title = models.CharField(max_length=200)
    inventory = models.ForeignKey(
        Inventory, on_delete=models.CASCADE,
        related_name='items', blank=True, null=True)
    character = models.ForeignKey(
        Character, on_delete=models.CASCADE,
        related_name='loot', blank=True, null=True)
    challenge = models.ForeignKey(
        Challenge, on_delete=models.CASCADE,
        related_name='loot', blank=True, null=True)
    value = models.IntegerField(default=0)
    item_type = models.CharField(
        max_length=32,
        choices=[
            (i_type.value, i_type) for i_type in ItemType],
        default=ItemType.SUPPLY)
    item_special = models.CharField(
        max_length=32,
        choices=[
            (s_type.value, s_type) for s_type in ItemSpecial],
        default=ItemSpecial.NONE)
    item_rarity = models.CharField(
        max_length=32,
        choices=[
            (r_type.value, r_type) for r_type in ItemRarity],
        default=ItemRarity.REGULAR)
    order_with_respect_to = 'inventory'
    image_str = models.CharField(max_length=200)

    class Meta(OrderedModel.Meta):
        pass

    @property
    def is_equipped(self):
        if self.invenotory:
            if self.inventory.owner:
                attributes = self.inventory.owner.attributes
                if self in [attributes.weapon, attributes.quiver,
                            attributes.legs_armor, attributes.head_armor,
                            attributes.torso_armor, attributes.shield_armor,
                            attributes.hands_armor]:
                    return True
        return False

    @property
    def market_price(self):
        fee = round(self.price / 100 * MARKET_FEE)
        return self.price + fee

    def __str__(self):
        return "{0} ({1})".format(self.title, self.inventory)
