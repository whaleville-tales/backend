from django.db import models
from whaleville.core.models.character.character import Character
from whaleville.core.models.game import Game
from whaleville.core.enums import InventoryType
from whaleville.core.models.base_models import UUIDModel
from config.settings.base import (
    MIN_INVENTORY_SLOTS, MAX_INVENTORY_SLOTS)


class Inventory(UUIDModel):
    owner = models.ForeignKey(
        Character,
        on_delete=models.CASCADE,
        blank=True, null=True,
        related_name='inventory'
    )
    game = models.ForeignKey(
        Game, on_delete=models.CASCADE,
        related_name='inventories')
    inventory_type = models.CharField(
        max_length=32,
        choices=[
            (i_type.value, i_type) for i_type in InventoryType],
        default=InventoryType.MARKET)
    funds = models.IntegerField(default=0)

    @property
    def max_slots(self):

        calculated_slots = self.owner.characteristics.constitution / 2
        if calculated_slots < MIN_INVENTORY_SLOTS:
            return MIN_INVENTORY_SLOTS
        elif calculated_slots > MAX_INVENTORY_SLOTS:
            return MAX_INVENTORY_SLOTS
        else:
            return calculated_slots

    @property
    def current_slots(self):
        return len(self.items.all())

    def __str__(self):
        if self.owner:
            return '{0} inventory in {1}'.format(self.owner.name, self.game)
        else:
            return '{0} inventory in {1}'.format(
                self.inventory_type, self.game)
