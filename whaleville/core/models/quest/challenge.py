from django.db import models
from ordered_model.models import OrderedModel
from whaleville.core.models.quest.quest import Quest
from whaleville.core.models.quest.challenge_data import ChallengeData


class Challenge(OrderedModel):
    quest = models.ForeignKey(
        Quest, on_delete=models.CASCADE,
        related_name='challenges')
    data_id = models.IntegerField()
    done = models.BooleanField(default=False)

    @property
    def description(self):
        return ChallengeData.objects.get(pk=self.data_id).description

    @property
    def success_challenge_description(self):
        return ChallengeData.objects.get(
            pk=self.data_id).success_challenge_description

    @property
    def failed_challenge_description(self):
        return ChallengeData.objects.get(
            pk=self.data_id).failed_challenge_description

    @property
    def challenge_type(self):
        return ChallengeData.objects.get(pk=self.data_id).challenge_type

    @property
    def difficulty(self):
        return ChallengeData.objects.get(pk=self.data_id).difficulty

    @property
    def damage_on_fail(self):
        return ChallengeData.objects.get(pk=self.data_id).damage_on_fail

    @property
    def target_type(self):
        return ChallengeData.objects.get(pk=self.data_id).target_type

    @property
    def skip_next(self):
        return ChallengeData.objects.get(pk=self.data_id).skip_next

    @property
    def title(self):
        return '{0}...({1}) in {2} of {3}'.format(
            self.description[:20], self.order,
            self.quest, self.quest.game)

    def __str__(self):
        return self.title
