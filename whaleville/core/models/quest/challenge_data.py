from django.db import models

from whaleville.core.enums import (ChallengeCheck, SkipNext,
                                   TargetType, ChallengeEnemy)
from whaleville.core.models.base_models import UUIDModel
from ordered_model.models import OrderedModel


class ChallengeData(OrderedModel, UUIDModel):
    quest_title = models.TextField()
    description = models.TextField()
    success_challenge_description = models.TextField(blank=True)
    failed_challenge_description = models.TextField(blank=True)
    challenge_type = models.CharField(
        max_length=32,
        choices=[
            (c_type.value, c_type) for c_type in ChallengeCheck],
        default=ChallengeCheck.NOTHING)
    difficulty = models.IntegerField(default=0)
    damage_on_fail = models.IntegerField(default=0)
    target_type = models.CharField(
        max_length=32,
        choices=[
            (t_type.value, t_type) for t_type in TargetType],
        default=TargetType.SELF)
    skip_next = models.CharField(
        max_length=32,
        choices=[
            (s_type.value, s_type) for s_type in SkipNext],
        default=SkipNext.NOTHING)
    is_lootable = models.BooleanField(default=False)
    enemies_quantity = models.IntegerField(default=0)
    enemies_type = models.CharField(
        max_length=32,
        choices=[
            (e_type.value, e_type) for e_type in ChallengeEnemy],
        default=ChallengeEnemy.NONE)

    @property
    def title(self):
        return '{0}...({1})'.format(
            self.description[:20], self.order)

    def __str__(self):
        return self.title
