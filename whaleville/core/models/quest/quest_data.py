from django.db import models
from django.contrib.postgres.fields import ArrayField
from whaleville.core.models.base_models import TimeStampedModel, UUIDModel


class QuestData(TimeStampedModel, UUIDModel):
    title = models.CharField(max_length=200)
    description = models.TextField()
    report = models.TextField()
    experience_value = models.IntegerField(default=0)
    reward_value = models.IntegerField(default=0)
    quest_chain = models.CharField(max_length=200)
    image_str = models.CharField(max_length=200)
    reveal_quests = ArrayField(
        models.CharField(max_length=200, blank=True),
        size=8,
        default=list,
        blank=True
    )

    def __str__(self):
        return self.title
