from django.contrib.auth import get_user_model
from django.db import models
from whaleville.core.models.base_models import UUIDModel
from whaleville.core.models.game import Game
from whaleville.core.enums import QuestType
from whaleville.core.models.quest.quest_data import QuestData


def get_sentinel_user():
    return get_user_model().objects.get_or_create(username='deleted')[0]


class QuestChain(UUIDModel):
    title = models.CharField(max_length=200)
    game = models.ForeignKey(
        Game, on_delete=models.CASCADE,
        related_name='quest_chains')

    @property
    def revealed_quests(self):
        return self.quests.exclude(quest_type=QuestType.HIDDEN.value).order_by(
            '-updated_at')

    @property
    def pending_quests(self):
        closed_quests = self.quests.filter(
            quest_type=QuestType.CLOSED.value).order_by(
            '-updated_at')
        all_quests = self.game.quests.all()
        pending_quests_list = []
        for quest in closed_quests:
            current_quest_data = QuestData.objects.get(pk=quest.data_id)
            quest_names = current_quest_data.reveal_quests
            for quest_name in quest_names:
                # check if name is non-empty
                if quest_name:
                    quest_data = QuestData.objects.get(
                        title=quest_name)
                    pending_quest = all_quests.get(
                        game=self.game,
                        data_id=quest_data.id)
                    if pending_quest.quest_type != QuestType.CLOSED.value and \
                        pending_quest not in list(
                            closed_quests):
                        pending_quests_list.append(pending_quest)
        return pending_quests_list

    def __str__(self):
        return '{0} in {1}'.format(self.title, self.game)
