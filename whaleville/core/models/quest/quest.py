from django.db import models
from whaleville.core.enums import QuestType
from whaleville.core.models.base_models import TimeStampedModel, UUIDModel
from whaleville.core.models.quest.quest_chain import QuestChain
from whaleville.core.models.quest.quest_data import QuestData
from whaleville.core.models.game import Game


class Quest(TimeStampedModel, UUIDModel):
    data_id = models.IntegerField()
    quest_type = models.CharField(
        max_length=32,
        choices=[
            (q_type.value, q_type) for q_type in QuestType],
        default=QuestType.HIDDEN)
    quest_chain = models.ForeignKey(
        QuestChain, on_delete=models.CASCADE,
        related_name='quests')
    game = models.ForeignKey(
        Game, on_delete=models.CASCADE,
        related_name='quests')

    @property
    def title(self):
        return QuestData.objects.get(pk=self.data_id).title

    @property
    def description(self):
        return QuestData.objects.get(pk=self.data_id).description

    @property
    def report(self):
        return QuestData.objects.get(pk=self.data_id).report

    @property
    def experience_value(self):
        return QuestData.objects.get(pk=self.data_id).experience_value

    @property
    def reward_value(self):
        return QuestData.objects.get(pk=self.data_id).reward_value

    @property
    def image_str(self):
        return QuestData.objects.get(pk=self.data_id).image_str

    def __str__(self):
        return '{0} in {1}'.format(self.title, self.game)
