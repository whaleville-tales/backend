from django.conf import settings
from django.contrib.auth import get_user_model
from django.db import models

from whaleville.core.utils import EditModelMixin
from whaleville.core.enums import QuestType


def get_sentinel_user():
    return get_user_model().objects.get_or_create(username='deleted')[0]


class Game(EditModelMixin, models.Model):
    created_date = models.DateTimeField(auto_now_add=True)
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        primary_key=True,
    )

    @property
    def is_finished(self):
        quests_left = self.quests.exclude(quest_type=QuestType.CLOSED.value)
        if quests_left:
            return False
        return True

    def __str__(self):
        return '{0} ({2} quests, started on {1})'.format(
            self.user, self.created_date.strftime(
                "%d %b %Y"), len(
                self.quests.exclude(quest_type=QuestType.HIDDEN.value)))
