
from django.db import models

from whaleville.core.models.quest.quest import Quest
from whaleville.core.enums import ActionLogType


class ActionLog(models.Model):
    quest = models.ForeignKey(
        Quest, on_delete=models.CASCADE, related_name='action_log')
    text = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    log_type = models.CharField(
        max_length=32,
        choices=[
            (a_type.value, a_type) for a_type in ActionLogType],
        default=ActionLogType.REGULAR.value)

    def __str__(self):
        return '{0}... {1}'.format(self.text[:20], self.log_type)
