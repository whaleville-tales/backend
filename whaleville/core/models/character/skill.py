from django.db import models

from whaleville.core.models.character.character import Character
from whaleville.core.enums import (
    SkillType, SkillSpecial, TargetType, CharacterClassType)


class Skill(models.Model):
    title = models.CharField(max_length=50)
    description = models.TextField(blank=True)
    level = models.IntegerField(default=1)
    value = models.IntegerField(default=0)
    is_hidden = models.BooleanField(default=True)
    character = models.ForeignKey(
        Character, on_delete=models.CASCADE,
        related_name='skills', blank=True, null=True)
    skill_type = models.CharField(
        max_length=32,
        choices=[
            (s_type.value, s_type) for s_type in SkillType],
        default=SkillType.DAMAGE)
    skill_special = models.CharField(
        max_length=32,
        choices=[
            (s_type.value, s_type) for s_type in SkillSpecial],
        default=SkillSpecial.NONE)
    target_type = models.CharField(
        max_length=32,
        choices=[
            (t_type.value, t_type) for t_type in TargetType],
        default=TargetType.SELF)
    class_type = models.CharField(
        max_length=32,
        choices=[
            (c_type.value, c_type) for c_type in CharacterClassType],
        default=CharacterClassType.ROGUE)

    def __str__(self):
        return '{0}({1}) for {2}'.format(
            self.title, self.skill_type, self.character.name)
