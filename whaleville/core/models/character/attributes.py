from django.db import models
from whaleville.core.utils import get_character_level, EditModelMixin
from whaleville.core.models.character.character import Character
from whaleville.core.enums import ItemType, ItemSpecial
from whaleville.core.processors.attributes import (
    get_best_weapon_for_use, get_best_relevant_armor, get_best_quiver_for_use,
    get_items_bonus_for_total)


class Attributes(EditModelMixin, models.Model):
    character = models.OneToOneField(
        Character,
        on_delete=models.CASCADE,
        primary_key=True,
    )
    experience = models.IntegerField(default=0)
    current_hp = models.IntegerField(default=0)
    current_exhaustion = models.IntegerField(default=0)
    armor_boost = models.IntegerField(default=0)

    @property
    def level(self):
        return get_character_level(int(self.experience))

    @property
    def damage(self):
        weapon_damage = 0
        boost_items_damage = 0
        strength = self.character.characteristics.strength
        boost_items = self.character.inventory.first().items.filter(
            item_type=ItemType.SUPPLY.value,
            item_special=ItemType.WEAPON.value)
        for item in boost_items:
            boost_items_damage += item.value
        if self.weapon:
            weapon_damage = self.weapon.value
            # calculate also arrows damage
            if self.weapon.item_special == ItemSpecial.BOW.value and self.quiver:  # noqa
                weapon_damage += self.quiver.value

        damage = 1 + strength / 10 + weapon_damage + boost_items_damage
        return round(damage)

    @property
    def quiver(self):
        inventory = self.character.inventory.first()
        all_quivers = inventory.items.filter(
            item_special=ItemSpecial.QUIVER.value)
        return get_best_quiver_for_use(all_quivers)

    @property
    def weapon(self):
        inventory = self.character.inventory.first()
        all_weapons = inventory.items.filter(
            item_type=ItemType.WEAPON.value).exclude(
            item_special=ItemSpecial.QUIVER.value)
        return get_best_weapon_for_use(all_weapons, self.quiver)

    @property
    def armor(self):
        armor = self.armor_boost
        boost_items_armor = 0
        boost_items = self.character.inventory.first().items.filter(
            item_type=ItemType.SUPPLY.value,
            item_special=ItemType.ARMOR.value)
        for item in boost_items:
            boost_items_armor += item.value
        if self.legs_armor:
            armor += self.legs_armor.value
        if self.torso_armor:
            armor += self.torso_armor.value
        if self.head_armor:
            armor += self.head_armor.value
        if self.shield_armor:
            armor += self.shield_armor.value
        if self.hands_armor:
            armor += self.hands_armor.value
        return armor + boost_items_armor

    @property
    def legs_armor(self):
        inventory = self.character.inventory.first()
        return get_best_relevant_armor(
            inventory.items.filter(
                item_type=ItemType.ARMOR.value,
                item_special=ItemSpecial.LEGS.value))

    @property
    def head_armor(self):
        inventory = self.character.inventory.first()
        return get_best_relevant_armor(
            inventory.items.filter(
                item_type=ItemType.ARMOR.value,
                item_special=ItemSpecial.HEAD.value))

    @property
    def torso_armor(self):
        inventory = self.character.inventory.first()
        return get_best_relevant_armor(
            inventory.items.filter(
                item_type=ItemType.ARMOR.value,
                item_special=ItemSpecial.TORSO.value))

    @property
    def shield_armor(self):
        inventory = self.character.inventory.first()
        return get_best_relevant_armor(
            inventory.items.filter(
                item_type=ItemType.ARMOR.value,
                item_special=ItemSpecial.SHIELD.value))

    @property
    def hands_armor(self):
        inventory = self.character.inventory.first()
        return get_best_relevant_armor(
            inventory.items.filter(
                item_type=ItemType.ARMOR.value,
                item_special=ItemSpecial.HANDS.value))

    @property
    def total_hp(self):
        inventory = self.character.inventory.first()
        bonus = get_items_bonus_for_total(
            inventory, ItemType.SUPPLY.value, ItemSpecial.HEALTH.value)
        # based on const
        return round(self.character.characteristics.constitution + bonus)

    @property
    def total_exhaustion(self):
        inventory = self.character.inventory.first()
        bonus = get_items_bonus_for_total(
            inventory, ItemType.SUPPLY.value, ItemSpecial.EXHAUSTION.value)
        return round(100 + bonus)

    def __str__(self):
        return self.character.name
