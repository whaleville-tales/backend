from django.db import models

from whaleville.core.models.game import Game
from whaleville.core.enums import (CharacterClassType,
                                   CharacterType, CharacterSexType)
from whaleville.core.models.quest.challenge import Challenge
from whaleville.core.models.base_models import UUIDModel


class Character(UUIDModel):
    game = models.ForeignKey(
        Game, on_delete=models.CASCADE,
        related_name='characters')
    challenge = models.ForeignKey(
        Challenge, on_delete=models.CASCADE,
        related_name='enemies', blank=True, null=True)
    name = models.CharField(max_length=200)
    bio = models.TextField(blank=True, null=True)
    price = models.IntegerField(default=0, blank=True, null=True)
    sex_type = models.CharField(
        max_length=32,
        choices=[
            (s_type.value, s_type) for s_type in CharacterSexType],
        default=CharacterSexType.MALE)
    class_type = models.CharField(
        max_length=32,
        choices=[
            (c_type.value, c_type) for c_type in CharacterClassType],
        default=CharacterClassType.ROGUE)
    character_type = models.CharField(
        max_length=32,
        choices=[
            (c_type.value, c_type) for c_type in CharacterType],
        default=CharacterType.GUILD)
    image_str = models.CharField(max_length=200, blank=True, null=True)
    joined_at = models.DateTimeField(auto_now_add=True)

    @property
    def revealed_skills(self):
        # fullfill dynamic name in skill description
        skills = self.skills.exclude(is_hidden=True)
        for skill in skills:
            skill.description = skill.description.format(self.name)
        return skills

    def __str__(self):
        return self.name
