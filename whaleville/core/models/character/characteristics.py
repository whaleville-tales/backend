from django.db import models

from whaleville.core.models.character.character import Character
from whaleville.core.utils import EditModelMixin
from whaleville.core.processors.attributes import get_blurred_attribute


class Characteristics(EditModelMixin, models.Model):
    character = models.OneToOneField(
        Character,
        on_delete=models.CASCADE,
        primary_key=True,
    )
    strength = models.IntegerField(default=0)
    dexterity = models.IntegerField(default=0)
    constitution = models.IntegerField(default=0)
    intelligence = models.IntegerField(default=0)
    perception = models.IntegerField(default=0)

    @property
    def blurred_strength(self):
        return get_blurred_attribute(self.strength)

    @property
    def blurred_dexterity(self):
        return get_blurred_attribute(self.dexterity)

    @property
    def blurred_constitution(self):
        return get_blurred_attribute(self.constitution)

    @property
    def blurred_intelligence(self):
        return get_blurred_attribute(self.intelligence)

    @property
    def blurred_perception(self):
        return get_blurred_attribute(self.perception)

    def __str__(self):
        return self.character.name
