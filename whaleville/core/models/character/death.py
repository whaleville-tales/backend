from django.db import models

from whaleville.core.models.character.character import Character


class Death(models.Model):
    character = models.OneToOneField(
        Character,
        on_delete=models.CASCADE,
        primary_key=True,
    )
    died_at = models.DateTimeField(auto_now_add=True)
    death_reason = models.CharField(max_length=200)

    @property
    def were_together(self):
        delta = self.died_at - self.character.joined_at
        return delta.days

    def __str__(self):
        return self.character.name
