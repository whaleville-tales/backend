from enum import Enum
from graphene import Enum as GrapheneEnum

# we are forced to recreate graph ql enum in order to avoid AssertionError
# https://github.com/graphql-python/graphene/issues/273


class QuestType(Enum):
    ACTIVE = "Active"
    COMPLETED = "Completed"
    CLOSED = "Closed"
    FAILED = "Failed"
    NEW = "New"
    HIDDEN = "Hidden"


class QuestTypeGraph(GrapheneEnum):
    ACTIVE = "Active"
    COMPLETED = "Completed"
    FAILED = "Failed"
    NEW = "New"


class EventType(Enum):
    QUESTCOMPLETE = "QuestComplete"
    LEVELUP = "Levelup"
    DEATH = "Death"
    NEWMEMBER = "NewMember"


class EventTypeGraph(GrapheneEnum):
    QUESTCOMPLETE = "QuestComplete"
    LEVELUP = "Levelup"
    DEATH = "Death"
    NEWMEMBER = "NewMember"


class ActionLogType(Enum):
    REGULAR = "Regular"
    DAMAGE = "Damage"
    LOOT = "Loot"
    DEATH = "Death"
    CHECK = "Check"
    SKILL = "Skill"
    ITEM = "Item"
    SUCCESS = "Success"
    FAIL = "Fail"


class ActionLogTypeGraph(GrapheneEnum):
    REGULAR = "Regular"
    DAMAGE = "Damage"
    LOOT = "Loot"
    DEATH = "Death"
    CHECK = "Check"
    SKILL = "Skill"
    ITEM = "Item"
    SUCCESS = "Success"
    FAIL = "Fail"


class CharacterSexType(Enum):
    MALE = "Male"
    FEMALE = "Female"


class CharacterClassType(Enum):
    ROGUE = "Rogue"
    WARRIOR = "Warrior"
    WIZARD = "Wizard"


class CharacterClassTypeGraph(GrapheneEnum):
    ROGUE = "Rogue"
    WARRIOR = "Warrior"
    WIZARD = "Wizard"


class CharacterType(Enum):
    GUILD = "Guild"
    PARTY = "Party"
    DEAD = "Dead"
    NPC = "NPC"
    ENEMY = "Enemy"


class CharacterTypeGraph(GrapheneEnum):
    GUILD = "Guild"
    PARTY = "Party"
    DEAD = "Dead"
    NPC = "NPC"
    ENEMY = "Enemy"


class SkillType(Enum):
    DAMAGE = "Damage"
    HEAL = "Heal"
    RESTORE = "Restore"
    ARMOR = "Armor"
    PASSIVE = "Passive"
    HIDDEN = "Hidden"


class SkillTypeGraph(GrapheneEnum):
    DAMAGE = "Damage"
    HEAL = "Heal"
    RESTORE = "Restore"
    ARMOR = "Armor"
    PASSIVE = "Passive"
    HIDDEN = "Hidden"


class SkillSpecial(Enum):
    DAMAGE = "Damage"
    DAMAGEABSORBTION = "DamageAbsorbtion"
    HEAL = "Heal"
    RESTORE = "Restore"
    NONE = "None"


class SkillSpecialGraph(GrapheneEnum):
    DAMAGE = "Damage"
    DAMAGEABSORBTION = "DamageAbsorbtion"
    HEAL = "Heal"
    RESTORE = "Restore"
    NONE = "None"


class TargetType(Enum):
    SELF = "Self"
    CURRENT = "Current"
    GROUP = "Group"


class TargetTypeGraph(GrapheneEnum):
    SELF = "Self"
    CURRENT = "Current"
    GROUP = "Group"


class InventoryType(Enum):
    MARKET = "Market"
    SHARED = "Shared"
    PRIVATE = "Private"


class InventoryTypeGraph(GrapheneEnum):
    MARKET = "Market"
    SHARED = "Shared"
    PRIVATE = "Private"


class ItemRarity(Enum):
    REGULAR = "Regular"
    RARE = "Rare"
    MASTERPIECE = "Masterpiece"
    LEGENDARY = "Legendary"


class ItemRarityGraph(GrapheneEnum):
    REGULAR = "Regular"
    RARE = "Rare"
    MASTERPIECE = "Masterpiece"
    LEGENDARY = "Legendary"


class ItemType(Enum):
    WEAPON = "Weapon"
    POTION = "Potion"
    ARMOR = "Armor"
    SUPPLY = "Supply"


class ItemTypeGraph(GrapheneEnum):
    WEAPON = "Weapon"
    POTION = "Potion"
    ARMOR = "Armor"
    SUPPLY = "Supply"


class ItemSpecial(Enum):
    SKILL = "Skill"
    MAGIC = "Magic"
    WEAPON = "Weapon"
    ARMOR = "Armor"
    HEALTH = "Health"
    EXHAUSTION = "Exhaustion"
    ACCURACY = "Accuracy"
    FOOD = "Food"
    LEGS = "Legs"
    HEAD = "Head"
    TORSO = "Torso"
    HANDS = "Hands"
    SHIELD = "Shield"
    BOW = "Bow"
    QUIVER = "Quiver"
    NONE = "None"


class ItemSpecialGraph(GrapheneEnum):
    SKILL = "Skill"
    MAGIC = "Magic"
    WEAPON = "Weapon"
    ARMOR = "Armor"
    HEALTH = "Health"
    EXHAUSTION = "Exhaustion"
    LEGS = "Legs"
    HEAD = "Head"
    TORSO = "Torso"
    SHIELD = "Shield"
    BOW = "Bow"
    QUIVER = "Quiver"
    NONE = "None"


class ChallengeCheck(Enum):
    NOTHING = "Nothing"
    STRENGTH = "Strength"
    DEXTERITY = "Dexterity"
    CONSTITUTION = "Constitution"
    INTELLIGENCE = "Intelligence"
    PERCEPTION = "Perception"


class SkipNext(Enum):
    NOTHING = "Nothing"
    ONEONSUCCESS = "OneOnSuccess"
    ONEONFAILURE = "OneOnFailure"
    TWOONSUCCESS = "TwoOnSuccess"
    TWOONFAILURE = "TwoOnFailure"


class ChallengeEnemy(Enum):
    NONE = "None"
    BANDIT = "Bandit"
    SMUGGLER = "Smuggler"
    CULTIST = "Cultist"
    INFECTED_CREATURE = "InfectedCreature"
    SEA_MONSTER = "SeaMonster"
    GIANT_SPIDER = "GiantSpider"
    WITCHER = "Witcher"
    FUNGUS = "Fungus"
