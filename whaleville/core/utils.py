from django.utils import timezone
from collections import namedtuple


class EditModelMixin(object):

    def edit(self, attrs, updated_date=False):
        for attr in attrs:
            if attr in self.__dict__ and attr not in ['id', 'uuid']:
                self.__dict__.update({attr: attrs[attr]})
        if updated_date:
            self.updated_date = timezone.now()
        self.save()


def get_character_level(experience_points):
    levels_list = []
    Level = namedtuple("Level", "experience level")
    EnhancedLevel = namedtuple("EnhancedLevel", "experience level next",)
    levels_list.append(Level(0, 1))
    levels_list.append(Level(250, 2))
    levels_list.append(Level(750, 3))
    levels_list.append(Level(1750, 4))
    levels_list.append(Level(4000, 5))
    levels_list.append(Level(9000, 6))
    levels_list.append(Level(14000, 7))
    levels_list.append(Level(20000, 8))
    levels_list.append(Level(27000, 9))
    levels_list.append(Level(35000, 10))
    levels_list.append(Level(45000, 11))

    if experience_points > 0:
        current_level = [level for level in levels_list if level.experience < experience_points][-1]  # noqa
    else:
        current_level = levels_list[0]
    next_level = levels_list[levels_list.index(current_level) + 1]
    return EnhancedLevel(
        current_level.experience,
        current_level.level, next_level.experience)
