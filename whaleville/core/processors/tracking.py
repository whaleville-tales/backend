import logging
from whaleville.core.models.action_log import ActionLog
from whaleville.core.resolvers.action_log.subscription import NewLogSubscription  # noqa


logger = logging.getLogger(__name__)


def log_action(quest, text, log_type=None):
    action_log = ActionLog()
    action_log.quest = quest
    action_log.text = text
    if log_type:
        action_log.log_type = log_type
    action_log.save()

    logger.info(text)
