import random
from whaleville.core.enums import ItemSpecial, CharacterClassType


def get_best_weapon_for_use(weapons, quiver):
    best_weapon = None
    if weapons:
        best_weapon = weapons[0]
        best_weapon_value = best_weapon.value
        if quiver:
            best_weapon_value = best_weapon.value + quiver.value

        for weapon in weapons:
            weapon_value = weapon.value
            if weapon.item_special == ItemSpecial.BOW.value\
                    and quiver and quiver.quantity > 0:
                weapon_value += quiver.value
            if weapon_value > best_weapon_value:
                best_weapon = weapon
    return best_weapon


def get_best_quiver_for_use(quivers):
    best_quiver = None
    if quivers:
        best_quiver = quivers[0]
        for quiver in quivers:
            if quiver.value > best_quiver.value:
                best_quiver = quiver
    return best_quiver


def get_best_relevant_armor(armors):
    if armors:
        best_armor = armors[0]
        for armor in armors:
            if armor.value > best_armor.value:
                best_armor = armor
        return best_armor
    else:
        return None


def get_items_bonus_for_total(inventory, item_type, item_special):
    """
    Calculate bonus for health or exhaustion total value
    """
    bonus = 0
    items = inventory.items.filter(
        item_type=item_type, item_special=item_special)
    for item in items:
        bonus += item.value
    return bonus


def learn_skill(character, level):
    hidden_skills_for_level = character.skills.filter(
        is_hidden=True, level=level)
    if hidden_skills_for_level:
        skill = random.choice(hidden_skills_for_level)
        skill.is_hidden = False
        skill.save()


def boost_characteristics(character):
    if character.class_type == CharacterClassType.WARRIOR.value:
        # gen for warrior
        character.characteristics.strength += random.randrange(4, 8)
        character.characteristics.dexterity += random.randrange(2, 5)
        character.characteristics.constitution += random.randrange(4, 8)
        character.characteristics.intelligence += random.randrange(2, 5)
        character.characteristics.perception += random.randrange(2, 5)
    if character.class_type == CharacterClassType.ROGUE.value:
        # gen for rogue
        character.characteristics.strength += random.randrange(3, 6)
        character.characteristics.dexterity += random.randrange(4, 7)
        character.characteristics.constitution += random.randrange(3, 7)
        character.characteristics.intelligence += random.randrange(3, 6)
        character.characteristics.perception += random.randrange(4, 7)
    if character.class_type == CharacterClassType.WIZARD.value:
        # gen for wizard
        character.characteristics.strength += random.randrange(2, 5)
        character.characteristics.dexterity += random.randrange(3, 6)
        character.characteristics.constitution += random.randrange(3, 7)
        character.characteristics.intelligence += random.randrange(4, 8)
        character.characteristics.perception += random.randrange(2, 5)
    character.characteristics.save()


def levelup(character, times):
    current_level = character.attributes.level.level
    for time in list(range(times)):
        boost_characteristics(character)
        learn_skill(character, (current_level - time))


def get_blurred_attribute(value):
    min_value = random.randrange(value - 2, value)
    max_value = random.randrange(value, value + 3)
    return '{0}-{1}'.format(min_value, max_value)
