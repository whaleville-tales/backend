import random
from whaleville.core.processors import combat, attributes, tracking
from whaleville.core.processors.generator import event_generator
from whaleville.core.enums import (
    CharacterClassType, ChallengeCheck, TargetType,
    InventoryType, CharacterType, ActionLogType)


def get_character_for_challenge(challenge_type, party):
    """
    Get most relevant character for the challenge:
    """
    character = party[0]
    if challenge_type in [
            ChallengeCheck.STRENGTH.value, ChallengeCheck.CONSTITUTION.value]:
        warrior_list = party.filter(
            class_type=CharacterClassType.WARRIOR.value)
        if warrior_list:
            character = warrior_list[0]
    if challenge_type in [
            ChallengeCheck.DEXTERITY.value, ChallengeCheck.PERCEPTION.value]:
        rogue_list = party.filter(
            class_type=CharacterClassType.ROGUE.value)
        if rogue_list:
            character = rogue_list[0]
    if challenge_type == ChallengeCheck.INTELLIGENCE.value:
        wizard_list = party.filter(
            class_type=CharacterClassType.WIZARD.value)
        if wizard_list:
            character = wizard_list[0]
    return character


def get_character_for_loot(party):
    """
    Get character with more free slots in private inventory
    """
    chosen = party[0]
    chosen_inventory = chosen.inventory.first()
    chosen_loading_ratio = (
        chosen_inventory.current_slots / chosen_inventory.max_slots)
    for character in party:
        character_inventory = character.inventory.first()
        character_loading_ratio = (
            character_inventory.current_slots / character_inventory.max_slots)  # noqa
        if character_loading_ratio < chosen_loading_ratio:
            chosen = character
    if chosen.inventory.first(
    ).current_slots == chosen.inventory.first().max_slots:
        return None
    return chosen


def check_challenge(
        characteristics_type, difficulty,
        chosen, items=None, skill_effect=None):
    base_percent = 40
    items_value = 0
    additional_effect = 0
    characteristics_value = 0
    if skill_effect:
        additional_effect = skill_effect
    if items:
        for item in items:
            items_value += item.value
    # check strength
    if characteristics_type == ChallengeCheck.STRENGTH.value:
        if chosen.class_type == CharacterClassType.WIZARD.value:
            base_percent = 30
        if chosen.class_type == CharacterClassType.WARRIOR.value:
            base_percent = 50
        characteristics_value = chosen.characteristics.strength
    # check dexterity
    if characteristics_type == ChallengeCheck.DEXTERITY.value:
        if chosen.class_type == CharacterClassType.WIZARD.value:
            base_percent = 30
        if chosen.class_type == CharacterClassType.ROGUE.value:
            base_percent = 50
        characteristics_value = chosen.characteristics.dexterity
    # check constitution
    if characteristics_type == ChallengeCheck.CONSTITUTION.value:
        if chosen.class_type == CharacterClassType.WIZARD.value:
            base_percent = 30
        if chosen.class_type == CharacterClassType.WARRIOR.value:
            base_percent = 50
        characteristics_value = chosen.characteristics.constitution
    # check intelligence
    if characteristics_type == ChallengeCheck.INTELLIGENCE.value:
        if chosen.class_type == CharacterClassType.WARRIOR.value:
            base_percent = 30
        if chosen.class_type == CharacterClassType.WIZARD.value:
            base_percent = 60
        characteristics_value = chosen.characteristics.intelligence
    # check awareness
    if characteristics_type == ChallengeCheck.PERCEPTION.value:
        if chosen.class_type == CharacterClassType.WIZARD.value:
            base_percent = 30
        if chosen.class_type == CharacterClassType.ROGUE.value:
            base_percent = 50
        characteristics_value = chosen.characteristics.perception
    total_percent = base_percent + additional_effect + \
        items_value + characteristics_value * 2 - difficulty * 2

    return random.randrange(100) < total_percent


def handle_challenge_fail(challenge, chosen, party):
    quest = challenge.quest
    text = '{0} зазнає невдачі'.format(
        chosen.name)
    tracking.log_action(quest, text, ActionLogType.FAIL.value)
    text = '{0}'.format(
        challenge.failed_challenge_description)
    tracking.log_action(quest, text)
    if challenge.damage_on_fail > 0:
        if challenge.target_type == TargetType.SELF.value:
            text = '{0} отримує {1} пошкоджень'.format(
                chosen.name, challenge.damage_on_fail)
            tracking.log_action(quest, text, ActionLogType.DAMAGE.value)
            combat.hit(chosen, challenge.damage_on_fail, quest)
        elif challenge.target_type == TargetType.GROUP.value:  # noqa
            text = 'Весь гурт отримує {0} пошкоджень'.format(
                challenge.damage_on_fail)
            tracking.log_action(quest, text, ActionLogType.DAMAGE.value)
            for target in party:
                combat.hit(
                    target, challenge.damage_on_fail, quest)


def get_loot(items, party, quest, source=None):
    for item in items:
        character = get_character_for_loot(party)
        if character:
            item.inventory = character.inventory.first()
            item.character = None
            item.challenge = None
            item.save()
            source_description = 'нишпорить оточенням і'
            if source:
                source_description = 'оглядає тіло {0} і'.format(source)
            text = '{0} {1} знаходить собі {2}'.format(
                character, source_description, item.title)
            tracking.log_action(quest, text, ActionLogType.LOOT.value)


def get_quest_reward(quest, game):
    shared_inventory = game.inventories.filter(
        inventory_type=InventoryType.SHARED.value).first()
    shared_inventory.funds += quest.reward_value
    shared_inventory.save()
    party = game.characters.filter(
        character_type=CharacterType.PARTY.value)
    for character in party:
        initial_level = character.attributes.level.level
        character.attributes.experience += quest.experience_value
        character.attributes.save()
        if character.attributes.level.level != initial_level:
            times = character.attributes.level.level - initial_level
            attributes.levelup(character, times)
            event_generator.handle_levelup_event(game, character)
