import random
from config.settings.base import APPS_DIR
from whaleville.core.models.character.character import Character
from whaleville.core.models.character.attributes import Attributes
from whaleville.core.models.character.characteristics import Characteristics
from whaleville.core.models.inventory.inventory import Inventory
from whaleville.core.processors import attributes
from whaleville.core.processors.generator import skills
from whaleville.core.processors.generator import items_generator
from whaleville.core.enums import (CharacterClassType, CharacterType,
                                   InventoryType, CharacterSexType)


def generate_character(game, name=None, character_type=None, class_type=None,
                       challenge=None, price=None, experience=None):
    sex_option = ['M', 'F']
    sex = random.choice(sex_option)
    party = game.characters.filter(character_type=CharacterType.PARTY.value)

    # random pick class type
    random_character_class = random.choice(list(CharacterClassType))

    character = Character()
    character.game = game
    character.name = generate_name(sex)
    character.bio = generate_bio(sex, party)
    character.character_type = CharacterType.PARTY.value
    character.class_type = random_character_class.value
    character.image_str = str(random.choice(list(range(1, 11))))
    if name:
        character.name = name
    if class_type:
        character.class_type = class_type
    if character_type:
        character.character_type = character_type
    if challenge:
        character.challenge = challenge
    if price:
        character.price = price
    if sex == 'F':
        character.sex_type = CharacterSexType.FEMALE.value
    elif sex == "M":
        character.sex_type = CharacterSexType.MALE.value

    character.save()
    # generate inventory
    inventory = Inventory()
    inventory.owner = character
    inventory.game = game
    inventory.inventory_type = InventoryType.PRIVATE.value
    inventory.save()
    # generate characteristics
    generate_characteristics(character)
    character.attributes = Attributes()
    # generate experience by mid level of current level
    character.attributes.experience = random.randrange(250)
    # generate skills
    skills.generate_skills(character)
    # learn skill for level 1
    attributes.learn_skill(character, 1)
    if experience:
        character.attributes.experience = experience
        attributes.levelup(character, character.attributes.level.level)
    character.attributes.current_hp = character.attributes.total_hp
    character.attributes.save()
    # generate items
    items_generator.generate_items_for_inventory(
        character.attributes.level.level, inventory, personal=True)
    return character


def generate_name(sex):
    if sex == 'M':
        filename = "male_names.txt"
    else:
        filename = "female_names.txt"
    return random.choice(open(
        '{0}/core/processors/generator/content/{1}'.format(
            APPS_DIR, filename)).readlines()).rstrip()


def generate_bio(sex, party):
    bio_list = list()
    if sex == 'M':
        filename = "male_bio.txt"
    else:
        filename = "female_bio.txt"
    if party:
        for character in party:
            bio_list.append(character.bio)
    bio = random.choice(open(
        '{0}/core/processors/generator/content/{1}'.format(
            APPS_DIR, filename)).readlines()).rstrip()
    while bio in bio_list:
        bio = random.choice(open(
            '{0}/core/processors/generator/content/{1}'.format(
                APPS_DIR, filename)).readlines()).rstrip()
    return bio


def generate_characteristics(character):
    character.characteristics = Characteristics()
    if character.class_type == CharacterClassType.WARRIOR.value:
        # gen for warrior
        character.characteristics.strength = random.randrange(10, 16)
        character.characteristics.dexterity = random.randrange(4, 11)
        character.characteristics.constitution = random.randrange(11, 19)
        character.characteristics.intelligence = random.randrange(4, 11)
        character.characteristics.perception = random.randrange(3, 11)
    if character.class_type == CharacterClassType.ROGUE.value:
        # gen for rogue
        character.characteristics.strength = random.randrange(7, 13)
        character.characteristics.dexterity = random.randrange(10, 16)
        character.characteristics.constitution = random.randrange(6, 13)
        character.characteristics.intelligence = random.randrange(4, 11)
        character.characteristics.perception = random.randrange(10, 16)
    if character.class_type == CharacterClassType.WIZARD.value:
        # gen for wizard
        character.characteristics.strength = random.randrange(4, 11)
        character.characteristics.dexterity = random.randrange(4, 11)
        character.characteristics.constitution = random.randrange(6, 13)
        character.characteristics.intelligence = random.randrange(12, 19)
        character.characteristics.perception = random.randrange(6, 13)
    character.characteristics.save()


def create_party(game):
    character_experience = random.randrange(500)
    for iteration in list(range(2)):
        generate_character(
            game=game, character_type=CharacterType.PARTY.value,
            experience=character_experience)
