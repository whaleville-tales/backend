from whaleville.core.models.event import Event
from whaleville.core.enums import EventType, CharacterSexType


def handle_death_event(game, character, description):
    event = Event(game=game, character=character,
                  event_type=EventType.DEATH.value,
                  description=description)
    event.save()


def handle_quest_complete_event(quest, game):
    event = Event(quest=quest, game=game, image_str=quest.image_str,
                  event_type=EventType.QUESTCOMPLETE.value,
                  description=quest.report)
    event.save()


def handle_levelup_event(game, character):
    description = "{0} переходить на {1} рівень".format(
        character.name, character.attributes.level.level)
    event = Event(game=game, character=character,
                  event_type=EventType.LEVELUP.value,
                  description=description)
    event.save()


def handle_new_member_event(game, character):
    if character.sex_type == CharacterSexType.MALE.value:
        sex_desc = "Він"
    else:
        sex_desc = "Вона"
    # add char desc with lowered first letter
    desc_list = character.bio.split(" ", 1)
    description = "{0} стає частиною гурту! {1} {2} {3}".format(
        character.name, sex_desc,
        desc_list[0].lower(), desc_list[1])
    event = Event(game=game, character=character,
                  event_type=EventType.NEWMEMBER.value,
                  description=description)
    event.save()
