import random
from config.settings.base import (APPS_DIR, RARE_ITEM_GENERATION_CHANCE,
                                  MASTERPIECE_ITEM_GENERATION_CHANCE,
                                  LEGENDARY_ITEM_GENERATION_CHANCE,
                                  MIN_MARKET_ITEMS_GENERATE,
                                  MAX_MARKET_ITEMS_GENERATE,
                                  MIN_CHARACTER_ITEMS_GENERATE,
                                  MAX_CHARACTER_ITEMS_GENERATE,
                                  LOOT_POTION_CHANCE)
from whaleville.core.models.inventory.item import Item
from whaleville.core.models.inventory.inventory import Inventory
from whaleville.core.enums import (ItemSpecial, ItemRarity,
                                   InventoryType, CharacterType)
from whaleville.core.processors.generator.items import (
    armor_items,
    boost_items,
    food_items,
    magic_items,
    potion_items,
    weapon_items,
    trade_items)


def generate_item(title, price, value, image_str, item_rarity=None,
                  challenge=None, item_type=None,
                  item_special=None, character=None, inventory=None,
                  quantity=None):
    item = Item()
    item.title = title
    item.price = price
    item.value = value
    item.image_str = image_str
    if item_rarity:
        item.item_rarity = item_rarity
    if challenge:
        item.challenge = challenge
    if inventory:
        item.inventory = inventory
    if character:
        item.character = character
    if item_special:
        item.item_special = item_special
    if item_type:
        item.item_type = item_type
    if quantity:
        item.quantity = quantity
    item.save()


def get_value_based_on_level_and_start_value(level, value):
    return round(random.uniform(value, level +
                                random.uniform(
                                    value, level / 2 * value)))


def get_price_based_on_level_and_start_price(level, start_price):
    return round(random.uniform(start_price, start_price +
                                random.uniform(start_price,
                                               start_price * level * 1.1)))


def get_supply_type_for_magic_items():
    supply_type_list = [
        ItemSpecial.HEALTH,
        ItemSpecial.EXHAUSTION,
        ItemSpecial.WEAPON,
        ItemSpecial.ARMOR,
        ItemSpecial.ACCURACY,
    ]
    return random.choice(supply_type_list)


def improve_item(level, start_price, start_value, title):
    improve_chance = random.randrange(100)
    if improve_chance < LEGENDARY_ITEM_GENERATION_CHANCE:
        price = get_price_based_on_level_and_start_price(
            level, start_price * 3)
        value = get_value_based_on_level_and_start_value(
            level, start_value * 2.5)
        item_rarity = ItemRarity.LEGENDARY.value
        adjective = random.choice(open(
            '{0}/core/processors/generator/content/legendary_items.txt'.format(  # noqa
                APPS_DIR)).readlines()).rstrip()
        title += ' {0}'.format(adjective)
    elif improve_chance < MASTERPIECE_ITEM_GENERATION_CHANCE:
        price = get_price_based_on_level_and_start_price(
            level, start_price * 2.2)
        value = get_value_based_on_level_and_start_value(
            level, start_value * 1.8)
        item_rarity = ItemRarity.MASTERPIECE.value
        adjective = random.choice(open(
            '{0}/core/processors/generator/content/masterpiece_items.txt'.format(  # noqa
                APPS_DIR)).readlines()).rstrip()
        title += ' {0}'.format(adjective)
    elif improve_chance < RARE_ITEM_GENERATION_CHANCE:
        price = get_price_based_on_level_and_start_price(
            level, start_price * 1.5)
        value = get_value_based_on_level_and_start_value(
            level, start_value * 1.5)
        item_rarity = ItemRarity.RARE.value
        adjective = random.choice(open(
            '{0}/core/processors/generator/content/rare_items.txt'.format(  # noqa
                APPS_DIR)).readlines()).rstrip()
        title += ' {0}'.format(adjective)
    else:
        price = get_price_based_on_level_and_start_price(
            level, start_price)
        value = get_value_based_on_level_and_start_value(
            level, start_value)
        item_rarity = ItemRarity.REGULAR.value
    return price, value, title, item_rarity


def get_items_bundle(personal=None):
    """
    Market items bundle  excludes trade items
    """
    initial_list = [armor_items.create_armor,
                    armor_items.create_bark_shield, armor_items.create_boot,
                    armor_items.create_glove, armor_items.create_helmet,
                    armor_items.create_iron_shield,
                    armor_items.create_wooden_shield,
                    boost_items.create_backpack, boost_items.create_compass,
                    boost_items.create_lantern, boost_items.create_map,
                    boost_items.create_rope, boost_items.create_tent,
                    boost_items.create_torch,
                    food_items.create_apple, food_items.create_beer,
                    food_items.create_beet, food_items.create_bread,
                    food_items.create_carrot, food_items.create_cheese,
                    food_items.create_fish, food_items.create_meat,
                    food_items.create_mushroom, food_items.create_potato,
                    magic_items.create_amulet, magic_items.create_ring,
                    potion_items.create_small_health_potion,
                    potion_items.create_middle_health_potion,
                    potion_items.create_big_health_potion,
                    potion_items.create_small_restoration_potion,
                    potion_items.create_middle_restoration_potion,
                    potion_items.create_big_restoration_potion,
                    weapon_items.create_axe, weapon_items.create_bow,
                    weapon_items.create_club, weapon_items.create_dagger,
                    weapon_items.create_hammer, weapon_items.create_lance,
                    weapon_items.create_mace, weapon_items.create_pickaxe,
                    weapon_items.create_pitchfork,
                    weapon_items.create_wooden_quiver,
                    weapon_items.create_copper_quiver,
                    weapon_items.create_silver_quiver,
                    weapon_items.create_adamant_quiver,
                    weapon_items.create_rapier,
                    weapon_items.create_stick, weapon_items.create_sword
                    ]
    if personal:
        initial_list.extend([trade_items.create_book,
                             trade_items.create_coin, trade_items.create_coal,
                             trade_items.create_crate,
                             trade_items.create_crystal,
                             trade_items.create_diamond,
                             trade_items.create_fur,
                             trade_items.create_honey,
                             trade_items.create_ingots,
                             trade_items.create_leather,
                             trade_items.create_ore,
                             trade_items.create_moldy_ore,
                             trade_items.create_purse,
                             trade_items.create_golden_skull,
                             trade_items.create_tooth,
                             ])
    return initial_list


def generate_items_for_inventory(level, inventory, personal=None):
    if personal:
        number_of_items = random.randrange(
            MIN_CHARACTER_ITEMS_GENERATE,
            MAX_CHARACTER_ITEMS_GENERATE)
    else:
        number_of_items = random.randrange(
            MIN_MARKET_ITEMS_GENERATE,
            MAX_MARKET_ITEMS_GENERATE)
    items = get_items_bundle(personal)
    for pick in list(range(0, number_of_items)):
        picked_item = random.choice(items)
        picked_item(level, inventory=inventory)


def update_market_items(game):
    market = game.inventories.filter(
        game=game, inventory_type=InventoryType.MARKET.value).first()
    party = game.characters.filter(
        game=game, character_type=CharacterType.PARTY.value)
    level = party[0].attributes.level.level
    # delete old items
    market.items.all().delete()
    generate_items_for_inventory(level, market)


def generate_loot_for_challenge(challenge):
    # thre is a big chance of healing potion
    if random.randrange(100) < LOOT_POTION_CHANCE:
        potions_list = [potion_items.create_small_health_potion,
                        potion_items.create_middle_health_potion,
                        potion_items.create_big_health_potion,
                        potion_items.create_small_restoration_potion,
                        potion_items.create_middle_restoration_potion,
                        potion_items.create_big_restoration_potion]
        picked_item = random.choice(potions_list)
        picked_item(1, challenge=challenge)
    else:
        items = get_items_bundle(True)
        picked_item = random.choice(items)
        picked_item(random.randint(1, 8), challenge=challenge)


def generate_loot_for_character(character):
    items = get_items_bundle(True)
    picked_item = random.choice(items)
    picked_item(
        random.randint(
            1,
            character.attributes.level.level),
        character=character)


def create_market(game):
    market = Inventory(game=game, inventory_type=InventoryType.MARKET.value)
    market.save()
    # populate market
    generate_items_for_inventory(1, market)


def create_shared_inventory(game):
    shared = Inventory(game=game, inventory_type=InventoryType.SHARED.value)
    shared.funds = random.randint(1, 500)
    shared.save()
    # populate shared inventory
    generate_items_for_inventory(1, shared, personal=True)
