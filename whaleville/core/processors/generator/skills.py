from whaleville.core.models.character.skill import Skill
from whaleville.core.enums import (SkillType, TargetType, CharacterClassType)


def generate_skills(character):
    if character.class_type == CharacterClassType.WIZARD.value:
        create_skill(
            character,
            1,
            'Удар громом',
            1,
            SkillType.DAMAGE.value,
            TargetType.GROUP.value,
            "Розкатистий оглушливий звук б'є по вушних перепонках ворогів")
        create_skill(
            character,
            1,
            'Зцілення ран',
            3,
            SkillType.HEAL.value,
            TargetType.SELF.value,
            "Від долонь {0} йде тепле світло, під яким гояться рани на тілі")
        create_skill(
            character,
            2,
            'Коло зцілення',
            2,
            SkillType.HEAL.value,
            TargetType.GROUP.value,
            "Навколо гурту ледь проявляється багряне коло. Невеликі рани затягуються самі собою")  # noqa
        create_skill(
            character,
            2,
            "Удар кам'яною брилою",
            4,
            SkillType.DAMAGE.value,
            TargetType.CURRENT.value,
            "За допомогою чар кам'яна брила поруч підіймається в повітря і несеться в напрямку ворога")  # noqa
        create_skill(
            character,
            3,
            "Блискавка",
            5,
            SkillType.DAMAGE.value,
            TargetType.CURRENT.value,
            "Повітря електризується і з нього вибухає бискава, що миттєво цілить в найближчого ворога")  # noqa
        create_skill(
            character,
            3,
            "Мерехтіння",
            3,
            SkillType.ARMOR.value,
            TargetType.GROUP.value,
            "Коли чари закінчили формуватися в просторі, на нетривалий проміжок часу кожен в гурті відчув, як почав помітно мерехтіти. Влучити по вас стає складніше. Захист зростає")  # noqa
        create_skill(
            character,
            4,
            "Промінь смерті",
            6,
            SkillType.DAMAGE.value,
            TargetType.CURRENT.value,
            "З-під пальців {0} вистрілює чоний їдкий промінь, який вражає найближчого ворога")  # noqa
        create_skill(
            character,
            4,
            "Відновлення сил",
            40,
            SkillType.RESTORE.value,
            TargetType.SELF.value,
            "Декілька майстерних магічних рухів відновлюють концентрацію та ясність думки")  # noqa
        create_skill(
            character,
            5,
            "Відображення",
            5,
            SkillType.ARMOR.value,
            TargetType.GROUP.value,
            "Для гурту поверхня деяких предметів навколо перетворюється на дзеркала. Несподівано обійти гурт з флангу тепер майже неможливо")  # noqa
        create_skill(
            character,
            5,
            "Сповільнена вогненна куля",
            5,
            SkillType.DAMAGE.value,
            TargetType.GROUP.value,
            "У просторі виникає куля з вогню діаметром до метра і рухається в сторону ворогів, спікаючи тих, не встиг відскочити")  # noqa
        create_skill(
            character,
            6,
            "Стіна ефіру",
            6,
            SkillType.ARMOR.value,
            TargetType.GROUP.value,
            "Між гуртом і ворогами з'являється невидима стіна, яку, попри те, дуже важко пошкодити")  # noqa
        create_skill(
            character,
            6,
            "Ланцюгова блискавка",
            6,
            SkillType.DAMAGE.value,
            TargetType.GROUP.value,
            "Електричний заряд вилітає з пальців {0} і б'є почергово всіх ворогів, рухаючись від цілі до цілі")  # noqa
        create_skill(
            character,
            7,
            "Сфера непроникності",
            7,
            SkillType.ARMOR.value,
            TargetType.GROUP.value,
            "Навколо гурту з'являється напівпрозора куля, яка поглинає велику частину пошкоджень")  # noqa
        create_skill(
            character,
            7,
            "Крижана завірюха",
            7,
            SkillType.DAMAGE.value,
            TargetType.GROUP.value,
            "Повітря навколо стає морозним і в його доторку з середовищем навколо витесується сніг та крига. Хуртовина набирає сил на б'є по стану опонентів")  # noqa
        create_skill(
            character,
            8,
            "Магічні обладунки",
            12,
            SkillType.ARMOR.value,
            TargetType.SELF.value,
            "Одяг {0} покривається нагору на вигляд скляними обладунками. Вони злегка світяться в темряві і вражаюче посилюють захист")  # noqa
        create_skill(
            character,
            8,
            "Хмара сарани",
            8,
            SkillType.DAMAGE.value,
            TargetType.GROUP.value,
            "Ви чуєте як десь позаду наростає незрозумілий гул. За мить ви розумієте, що це незліченна кількість сарани, яка мчить повз вас і атакує ворогів")  # noqa
        create_skill(
            character,
            9,
            "Ментальний шок",
            13,
            SkillType.DAMAGE.value,
            TargetType.CURRENT.value,
            "Обравши ціль, {0} скеровує величезний потік псіонічної енергії прямо в розум ворога. Ціль кричить і корчиться в конвульсіях")  # noqa
        create_skill(
            character,
            9,
            "Заспокоєння ментальних тіл",
            25,
            SkillType.RESTORE.value,
            TargetType.GROUP.value,
            "{0} намагається заспокоїть ефірні вібрації союзників, які бринять і розходяться деміпростором набагато далі ніж, сягає око. Кожен у гурті почувається більш врівноваженим")  # noqa
        create_skill(
            character,
            10,
            "Приплив життя",
            10,
            SkillType.HEAL.value,
            TargetType.GROUP.value,
            "Кожен в гурті відчуває, як увібрані тілом пошкодження розчиняються і зникають, мовби їх ніколи не було")  # noqa
        create_skill(
            character,
            10,
            "Приплив смерті",
            10,
            SkillType.DAMAGE.value,
            TargetType.GROUP.value,
            "Хвилі болю і страждань накочують на ваших ворогів. Потрібен гарт тіла та духу, аби після подібного лишатися стояти")  # noqa
    elif character.class_type == CharacterClassType.WARRIOR.value:
        create_skill(
            character,
            1,
            'Удар коліном',
            3,
            SkillType.DAMAGE.value,
            TargetType.CURRENT.value,
            "Користуючись можливістю, {0} б'є ворога коліном")
        create_skill(
            character,
            1,
            'Удар ліктем ',
            3,
            SkillType.DAMAGE.value,
            TargetType.CURRENT.value,
            "Користуючись можливістю, {0} б'є ворога своїм твердим гострим ліктем")  # noqa
        create_skill(
            character,
            2,
            'Промивання рани',
            4,
            SkillType.HEAL.value,
            TargetType.SELF.value,
            "Швидко і професійно {0} промиває рану чистою водою, що відразу покращує фізичний стан")  # noqa
        create_skill(
            character,
            2,
            "Кидок каменем",
            4,
            SkillType.DAMAGE.value,
            TargetType.CURRENT.value,
            "{0} знаходить під рукою дуже зручний камінь для метання і відразу ж ним користується")  # noqa
        create_skill(
            character,
            3,
            "Рвучкі рухи",
            5,
            SkillType.ARMOR.value,
            TargetType.SELF.value,
            "{0} починає рухатися різко й асинхронно, що ускладнює можливість завдати прицільного удару")  # noqa
        create_skill(
            character,
            3,
            "Точний удар",
            5,
            SkillType.DAMAGE.value,
            TargetType.CURRENT.value,
            "Зосередившись на цілі, {0} безпомилково визначає слабке місце і наносить удар")  # noqa
        create_skill(
            character,
            4,
            "Ситуативний щит",
            6,
            SkillType.ARMOR.value,
            TargetType.SELF.value,
            "{0} використовує навколишнє середовище як захист")  # noqa
        create_skill(
            character,
            4,
            "Ситуативні барикади",
            4,
            SkillType.ARMOR.value,
            TargetType.GROUP.value,
            "{0} наспіх споруджує барикади з каменю та дошок, що були знайдені неподалік")  # noqa
        create_skill(
            character,
            5,
            "Стрибок пантери",
            8,
            SkillType.DAMAGE.value,
            TargetType.CURRENT.value,
            "Використовуючи древній східний прийом, {0} відштовхується від вертикальної основи та атакує свою ціль")  # noqa
        create_skill(
            character,
            5,
            "Запікання ран",
            8,
            SkillType.HEAL.value,
            TargetType.SELF.value,
            "Користуючись можливістю {0} розжарює лезо та запікає ним власні відкриті рани")  # noqa
        create_skill(
            character,
            6,
            "Захисні руни",
            9,
            SkillType.ARMOR.value,
            TargetType.SELF.value,
            "{0} підносить перед собою руки і читає руни, витатуюйовані на на них. З кожною прочитаною руною {0} опановує якусь невидиму твердь")  # noqa
        create_skill(
            character,
            6,
            "Захисний зв'язок",
            6,
            SkillType.ARMOR.value,
            TargetType.GROUP.value,
            "Ви пробули в гурті достатньо довго, аби налаштуватися одне на одного. Тепер захищатися буде легше")  # noqa
        create_skill(
            character,
            7,
            "Випалена шкіра",
            10,
            SkillType.ARMOR.value,
            TargetType.SELF.value,
            "Тисячі тренувань привели до того, що навіть шкіра в бою тепер стає твердою як крис")  # noqa
        create_skill(
            character,
            7,
            "Мистецтво бою",
            10,
            SkillType.DAMAGE.value,
            TargetType.CURRENT.value,
            "Гарт тренувань та майстерність вчителів зрештою дали свої плоди. Бій для {0} - у першу чергу, мистецтво")  # noqa
        create_skill(
            character,
            8,
            "Тактичний маневр",
            12,
            SkillType.ARMOR.value,
            TargetType.SELF.value,
            "{0} використовує знання тактики аби максимально захиститися в цій ситуації")  # noqa
        create_skill(
            character,
            8,
            "Руни регенерації",
            12,
            SkillType.HEAL.value,
            TargetType.SELF.value,
            "{0} читає руни зі своїх татуювань і пошкоджені клітии починають відновлюватися прямо на очах")  # noqa
        create_skill(
            character,
            9,
            "Ефірний клинок",
            13,
            SkillType.DAMAGE.value,
            TargetType.CURRENT.value,
            "В повітрі з'являється осяйний меч, який рухається автономно і атакує поточну ціль")  # noqa
        create_skill(
            character,
            9,
            "Глибокі рани",
            13,
            SkillType.HEAL.value,
            TargetType.SELF.value,
            "Користуючись власним досвідом та досвідом сотень воїнів, залишеним у великих книгах, {0} зупиняє кров та зашиває пошкодження")  # noqa
        create_skill(
            character,
            10,
            "Тваринний жах",
            10,
            SkillType.DAMAGE.value,
            TargetType.GROUP.value,
            "Своєю репутацією, рухами, вигуками та холодною кригою в очах ви наводите на ворогів панічного жаху. Вороги отримують пошкодження псіонічного характеру")  # noqa
        create_skill(
            character,
            10,
            "Часткове кам'яніння",
            14,
            SkillType.ARMOR.value,
            TargetType.SELF.value,
            "Через розуміння мистецтво бою та древніх рун воїнів, {0} осягає здібності до часткової метаморфози верхніх шарів власної шкіри")  # noqa
    elif character.class_type == CharacterClassType.ROGUE.value:
        create_skill(
            character,
            1,
            'Перевага місцевості',
            1,
            SkillType.ARMOR.value,
            TargetType.GROUP.value,
            "{0} аналізує місцевість і вказує решті зайняти найвигідніші позиції")  # noqa
        create_skill(
            character,
            1,
            'Постріл пращею',
            3,
            SkillType.DAMAGE.value,
            TargetType.CURRENT.value,
            "Підібравши момент, {0} дістає з кишені пращу та стріляє гострим каменем по цілі")  # noqa
        create_skill(
            character,
            2,
            'Цілющі трави',
            4,
            SkillType.HEAL.value,
            TargetType.SELF.value,
            "{0} впізнає кілька листочків на землі та прикладає їх до свіжих ран")  # noqa
        create_skill(
            character,
            2,
            "Спритність рук",
            4,
            SkillType.ARMOR.value,
            TargetType.SELF.value,
            "{0} рухається набагато швидше, ніж на це розраховував ворог. Це перетворюється на додатковий захист")  # noqa
        create_skill(
            character,
            3,
            "Цілющі слова",
            3,
            SkillType.HEAL.value,
            TargetType.GROUP.value,
            "{0} славиться тим, що може заспокоїти та врівноважити навіть простими словами. Зараз вони зцілюють союзників")  # noqa
        create_skill(
            character,
            3,
            "Завіса стріл",
            3,
            SkillType.DAMAGE.value,
            TargetType.GROUP.value,
            "На заклик {0} нізвідки з'являється завіса стріл, яка летить в керунку ворога")  # noqa
        create_skill(
            character,
            4,
            "Хітинові нарости",
            6,
            SkillType.ARMOR.value,
            TargetType.SELF.value,
            "Провівши життя відлюдника у цілковитій єдності з природою тепер {0} має змогу за потреби вкривати власне тіло хітиновим панцирем")  # noqa
        create_skill(
            character,
            4,
            "Ефірний обстріл",
            4,
            SkillType.DAMAGE.value,
            TargetType.GROUP.value,
            "Ледь видимі стріли освітлюють своїм світлим блиском місцину і несуться у ворожу сторону")  # noqa
        create_skill(
            character,
            5,
            "Майстерний випад",
            8,
            SkillType.DAMAGE.value,
            TargetType.CURRENT.value,
            "Після років у вищому  товаристві, {0} має певні навички фехтування")  # noqa
        create_skill(
            character,
            5,
            "Цілющі води",
            25,
            SkillType.RESTORE.value,
            TargetType.GROUP.value,
            "{0} вивчає місцеву водойму і несподівано розуміє, що властивості цієї води виходять за межі звичайної")  # noqa
        create_skill(
            character,
            6,
            "Молитва зцілення",
            9,
            SkillType.HEAL.value,
            TargetType.SELF.value,
            "{0} має прихильників на небі, тому такі невеличкі потреби завжди лишається почутими")  # noqa
        create_skill(
            character,
            6,
            "Хмара кинджалів",
            6,
            SkillType.DAMAGE.value,
            TargetType.GROUP.value,
            "Десятки металевих лез розтинають повітря і несуться в сторону ворога")  # noqa
        create_skill(
            character,
            7,
            "Надшвидкі рухи",
            10,
            SkillType.DAMAGE.value,
            TargetType.CURRENT.value,
            "Маючи розвинену від природи кмітливість {0} вправно використовує її в бою")  # noqa
        create_skill(
            character,
            7,
            "Медитація",
            40,
            SkillType.RESTORE.value,
            TargetType.SELF.value,
            "{0} вміє відновлюватися швидко й ефективно завдяки тривалому відлюдному життю")  # noqa
        create_skill(
            character,
            8,
            "Передбачення",
            12,
            SkillType.ARMOR.value,
            TargetType.SELF.value,
            "{0} чітко уявляє наступний рух суперника, тому випереджає його та атакує заздалегідь")  # noqa
        create_skill(
            character,
            8,
            "Гострий дротик",
            12,
            SkillType.DAMAGE.value,
            TargetType.CURRENT.value,
            "{0} має в кишенях дрібку сюрпризів для своїх ворогів, зокрема вдало кинутий дротик може змінити перебіг бою")  # noqa
        create_skill(
            character,
            9,
            "Огорожа з терену",
            9,
            SkillType.ARMOR.value,
            TargetType.GROUP.value,
            "{0} нашіптує щось навколишнім кущикам і вже за мить ви бачите як навколо гурту виростає справжня захисна стіна з терену")  # noqa
        create_skill(
            character,
            9,
            "Омана",
            13,
            SkillType.DAMAGE.value,
            TargetType.CURRENT.value,
            "{0} проявляє хитрість та вдає, що знемагає. Але в миьт, коли ворог послаблює, {0} атакує")  # noqa
        create_skill(
            character,
            10,
            "Поезія життя",
            10,
            SkillType.HEAL.value,
            TargetType.GROUP.value,
            "Маючи владу та силу слова, {0} викоритовує її аби зцілити гурт")  # noqa
        create_skill(
            character,
            10,
            "Поезія смерті",
            10,
            SkillType.DAMAGE.value,
            TargetType.GROUP.value,
            "Маючи владу та силу слова, {0} викоритовує її аби знищити ворогів")  # noqa


def create_skill(character, level, title, value, skill_type,
                 target_type, description):
    skill = Skill()
    skill.character = character
    skill.title = title
    skill.value = value
    skill.skill_type = skill_type
    skill.target_type = target_type
    skill.level = level
    skill.description = description
    skill.save()
