from whaleville.core.processors.generator import items_generator
from whaleville.core.enums import ItemType, ItemSpecial, ItemRarity


def create_food(title, price, value, image_str,
                challenge=None,
                character=None, inventory=None):
    item_type = ItemType.SUPPLY.value
    item_special = ItemSpecial.FOOD.value
    item_rarity = ItemRarity.REGULAR.value

    items_generator.generate_item(title=title, price=price, value=value,
                                  image_str=image_str, item_type=item_type,
                                  item_special=item_special,
                                  item_rarity=item_rarity,
                                  challenge=challenge, character=character,
                                  inventory=inventory)


def create_apple(level,
                 challenge=None,
                 character=None, inventory=None):
    title = 'Яблуко'
    price = 10
    value = 3
    image_str = 'apple'

    create_food(title=title, price=price, value=value,
                image_str=image_str,
                challenge=challenge, character=character,
                inventory=inventory)


def create_beer(level,
                challenge=None,
                character=None, inventory=None):
    title = 'Пиво'
    price = 5
    value = 3
    image_str = 'beer'

    create_food(title=title, price=price, value=value,
                image_str=image_str,
                challenge=challenge, character=character,
                inventory=inventory)


def create_beet(level,
                challenge=None,
                character=None, inventory=None):
    title = 'Буряк'
    price = 5
    value = 1
    image_str = 'beet'

    create_food(title=title, price=price, value=value,
                image_str=image_str,
                challenge=challenge, character=character,
                inventory=inventory)


def create_bread(level,
                 challenge=None,
                 character=None, inventory=None):
    title = 'Хліб'
    price = 10
    value = 2
    image_str = 'bread'

    create_food(title=title, price=price, value=value,
                image_str=image_str,
                challenge=challenge, character=character,
                inventory=inventory)


def create_carrot(level,
                  challenge=None,
                  character=None, inventory=None):
    title = 'Морква'
    price = 5
    value = 1
    image_str = 'carrot'

    create_food(title=title, price=price, value=value,
                image_str=image_str,
                challenge=challenge, character=character,
                inventory=inventory)


def create_cheese(level,
                  challenge=None,
                  character=None, inventory=None):
    title = 'Сир'
    price = 10
    value = 2
    image_str = 'cheese'

    create_food(title=title, price=price, value=value,
                image_str=image_str,
                challenge=challenge, character=character,
                inventory=inventory)


def create_fish(level,
                challenge=None,
                character=None, inventory=None):
    title = 'Риба'
    price = 5
    value = 2
    image_str = 'fish'

    create_food(title=title, price=price, value=value,
                image_str=image_str,
                challenge=challenge, character=character,
                inventory=inventory)


def create_meat(level,
                challenge=None,
                character=None, inventory=None):
    title = "М'ясо"
    price = 10
    value = 3
    image_str = 'meat'

    create_food(title=title, price=price, value=value,
                image_str=image_str,
                challenge=challenge, character=character,
                inventory=inventory)


def create_mushroom(level,
                    challenge=None,
                    character=None, inventory=None):
    title = "Гриб"
    price = 5
    value = 1
    image_str = 'mushroom'

    create_food(title=title, price=price, value=value,
                image_str=image_str,
                challenge=challenge, character=character,
                inventory=inventory)


def create_potato(level,
                  challenge=None,
                  character=None, inventory=None):
    title = "Картопля"
    price = 5
    value = 1
    image_str = 'potato'

    create_food(title=title, price=price, value=value,
                image_str=image_str,
                challenge=challenge, character=character,
                inventory=inventory)


def create_star_dust(level,
                     challenge=None,
                     character=None, inventory=None):
    title = "Зоряний Пил"
    price = 1000
    value = 80
    image_str = 'stardust'

    create_food(title=title, price=price, value=value,
                image_str=image_str,
                challenge=challenge, character=character,
                inventory=inventory)
