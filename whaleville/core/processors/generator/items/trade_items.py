from whaleville.core.processors.generator import items_generator
from whaleville.core.enums import ItemType, ItemSpecial


def create_trade_item(level, title, image_str, price,
                      challenge=None,
                      character=None, inventory=None):
    item_type = ItemType.SUPPLY.value
    item_special = ItemSpecial.NONE.value
    price, value, title, item_rarity = items_generator.improve_item(
        level, price, 0, title)

    items_generator.generate_item(title=title, price=price, value=0,
                                  image_str=image_str, item_type=item_type,
                                  item_special=item_special,
                                  item_rarity=item_rarity,
                                  challenge=challenge, character=character,
                                  inventory=inventory)


def create_book(level,
                challenge=None,
                character=None, inventory=None):
    image_str = 'book'
    price = 250
    title = 'Книга'

    create_trade_item(level=level, title=title, price=price,
                      image_str=image_str,
                      challenge=challenge, character=character,
                      inventory=inventory)


def create_coal(level,
                challenge=None,
                character=None, inventory=None):
    image_str = 'coal'
    price = 50
    title = 'Шмат вугілля'

    create_trade_item(level=level, title=title, price=price,
                      image_str=image_str,
                      challenge=challenge, character=character,
                      inventory=inventory)


def create_coin(level,
                challenge=None,
                character=None, inventory=None):
    image_str = 'coin'
    price = 150
    title = 'Монета невідомого походження'

    create_trade_item(level=level, title=title, price=price,
                      image_str=image_str,
                      challenge=challenge, character=character,
                      inventory=inventory)


def create_crate(level,
                 challenge=None,
                 character=None, inventory=None):
    image_str = 'crate'
    price = 200
    title = 'Ящик з провізією'

    create_trade_item(level=level, title=title, price=price,
                      image_str=image_str,
                      challenge=challenge, character=character,
                      inventory=inventory)


def create_crystal(level,
                   challenge=None,
                   character=None, inventory=None):
    image_str = 'crystal'
    price = 500
    title = 'Кристал'

    create_trade_item(level=level, title=title, price=price,
                      image_str=image_str,
                      challenge=challenge, character=character,
                      inventory=inventory)


def create_diamond(level,
                   challenge=None,
                   character=None, inventory=None):
    image_str = 'diamond'
    price = 700
    title = 'Алмаз'

    create_trade_item(level=level, title=title, price=price,
                      image_str=image_str,
                      challenge=challenge, character=character,
                      inventory=inventory)


def create_fur(level,
               challenge=None,
               character=None, inventory=None):
    image_str = 'fur'
    price = 100
    title = 'Хутро'

    create_trade_item(level=level, title=title, price=price,
                      image_str=image_str,
                      challenge=challenge, character=character,
                      inventory=inventory)


def create_honey(level,
                 challenge=None,
                 character=None, inventory=None):
    image_str = 'honey'
    price = 80
    title = 'Мед'

    create_trade_item(level=level, title=title, price=price,
                      image_str=image_str,
                      challenge=challenge, character=character,
                      inventory=inventory)


def create_ingots(level,
                  challenge=None,
                  character=None, inventory=None):
    image_str = 'ingots'
    price = 250
    title = 'Металеві злитки'

    create_trade_item(level=level, title=title, price=price,
                      image_str=image_str,
                      challenge=challenge, character=character,
                      inventory=inventory)


def create_leather(level,
                   challenge=None,
                   character=None, inventory=None):
    image_str = 'leather'
    price = 100
    title = 'Шкіра'

    create_trade_item(level=level, title=title, price=price,
                      image_str=image_str,
                      challenge=challenge, character=character,
                      inventory=inventory)


def create_ore(level,
               challenge=None,
               character=None, inventory=None):
    image_str = 'ore'
    price = 300
    title = 'Руда'

    create_trade_item(level=level, title=title, price=price,
                      image_str=image_str,
                      challenge=challenge, character=character,
                      inventory=inventory)


def create_moldy_ore(level,
                     challenge=None,
                     character=None, inventory=None):
    image_str = 'moldy_ore'
    price = 40
    title = 'Запліснявіла руда'

    create_trade_item(level=level, title=title, price=price,
                      image_str=image_str,
                      challenge=challenge, character=character,
                      inventory=inventory)


def create_purse(level,
                 challenge=None,
                 character=None, inventory=None):
    title = "Гаманець з монетами"
    price = 100
    image_str = 'purse'

    create_trade_item(level=level, title=title, price=price,
                      image_str=image_str,
                      challenge=challenge, character=character,
                      inventory=inventory)


def create_golden_skull(level,
                        challenge=None,
                        character=None, inventory=None):
    title = "Золотий череп"
    price = 1000
    image_str = 'skull'

    create_trade_item(level=level, title=title, price=price,
                      image_str=image_str,
                      challenge=challenge, character=character,
                      inventory=inventory)


def create_tooth(level,
                 challenge=None,
                 character=None, inventory=None):
    title = "Зуб"
    price = 20
    image_str = 'tooth'

    create_trade_item(level=level, title=title, price=price,
                      image_str=image_str,
                      challenge=challenge, character=character,
                      inventory=inventory)
