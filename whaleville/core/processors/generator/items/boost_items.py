from whaleville.core.processors.generator import items_generator
from whaleville.core.enums import ItemType, ItemSpecial


def create_boost_item(level, title, image_str, price, value,
                      item_special, challenge=None,
                      character=None, inventory=None):
    item_type = ItemType.SUPPLY.value
    price, value, title, item_rarity = items_generator.improve_item(
        level, price, value, title)

    items_generator.generate_item(title=title, price=price, value=value,
                                  image_str=image_str, item_type=item_type,
                                  item_special=item_special,
                                  item_rarity=item_rarity,
                                  challenge=challenge, character=character,
                                  inventory=inventory)


def create_backpack(level,
                    challenge=None,
                    character=None, inventory=None):
    title = 'Наплічник'
    image_str = 'backpack'
    item_special = ItemSpecial.EXHAUSTION.value
    price = 50
    value = 15

    create_boost_item(level=level, title=title, price=price, value=value,
                      image_str=image_str, item_special=item_special,
                      challenge=challenge, character=character,
                      inventory=inventory)


def create_compass(level,
                   challenge=None,
                   character=None, inventory=None):
    image_str = 'compass'
    item_special = ItemSpecial.EXHAUSTION.value
    title = 'Компас'
    price = 200
    value = 10

    create_boost_item(level=level, title=title, price=price, value=value,
                      image_str=image_str, item_special=item_special,
                      challenge=challenge, character=character,
                      inventory=inventory)


def create_lantern(level,
                   challenge=None,
                   character=None, inventory=None):
    title = 'Ліхтар'
    image_str = 'lantern'
    item_special = ItemSpecial.EXHAUSTION.value
    price = 200
    value = 20

    create_boost_item(level=level, title=title, price=price, value=value,
                      image_str=image_str, item_special=item_special,
                      challenge=challenge, character=character,
                      inventory=inventory)


def create_map(level,
               challenge=None,
               character=None, inventory=None):
    title = 'Карта'
    image_str = 'map'
    item_special = ItemSpecial.EXHAUSTION.value
    price = 200
    value = 10

    create_boost_item(level=level, title=title, price=price, value=value,
                      image_str=image_str, item_special=item_special,
                      challenge=challenge, character=character,
                      inventory=inventory)


def create_rope(level,
                challenge=None,
                character=None, inventory=None):
    title = 'Мотузка'
    image_str = 'rope'
    item_special = ItemSpecial.EXHAUSTION.value
    price = 200
    value = 10

    create_boost_item(level=level, title=title, price=price, value=value,
                      image_str=image_str, item_special=item_special,
                      challenge=challenge, character=character,
                      inventory=inventory)


def create_sleeping_bag(level,
                        challenge=None,
                        character=None, inventory=None):
    title = 'Спальник'
    image_str = 'sleeping_bag'
    item_special = ItemSpecial.EXHAUSTION.value
    price = 350
    value = 20

    create_boost_item(level=level, title=title, price=price, value=value,
                      image_str=image_str, item_special=item_special,
                      challenge=challenge, character=character,
                      inventory=inventory)


def create_tent(level,
                challenge=None,
                character=None, inventory=None):
    title = 'Намет'
    image_str = 'tent'
    item_special = ItemSpecial.EXHAUSTION.value
    price = 500
    value = 30

    create_boost_item(level=level, title=title, price=price, value=value,
                      image_str=image_str, item_special=item_special,
                      challenge=challenge, character=character,
                      inventory=inventory)


def create_torch(level,
                 challenge=None,
                 character=None, inventory=None):
    title = 'Факел'
    image_str = 'torch'
    item_special = ItemSpecial.EXHAUSTION.value
    price = 100
    value = 10

    create_boost_item(level=level, title=title, price=price, value=value,
                      image_str=image_str, item_special=item_special,
                      challenge=challenge, character=character,
                      inventory=inventory)
