from whaleville.core.processors.generator import items_generator
from whaleville.core.enums import ItemType, ItemSpecial


def create_armor_items(level, title, image_str, price, value,
                       item_special, challenge=None,
                       character=None, inventory=None):
    item_type = ItemType.ARMOR.value
    price, value, title, item_rarity = items_generator.improve_item(
        level, price, value, title)

    items_generator.generate_item(title=title, price=price, value=value,
                                  image_str=image_str, item_type=item_type,
                                  item_special=item_special,
                                  item_rarity=item_rarity,
                                  challenge=challenge, character=character,
                                  inventory=inventory)


def create_armor(level, title=None,
                 challenge=None,
                 character=None, inventory=None):
    title = 'Обладунки'
    if title:
        title = title
    image_str = 'armor'
    price = 100
    value = 5
    item_special = ItemSpecial.TORSO.value

    create_armor_items(level=level, title=title, price=price, value=value,
                       image_str=image_str, item_special=item_special,
                       challenge=challenge, character=character,
                       inventory=inventory)


def create_bark_shield(level,
                       challenge=None,
                       character=None, inventory=None):
    title = 'Абиякий щит'
    image_str = 'bark'
    item_special = ItemSpecial.SHIELD.value
    price = 25
    value = 1

    create_armor_items(level=level, title=title, price=price, value=value,
                       image_str=image_str, item_special=item_special,
                       challenge=challenge, character=character,
                       inventory=inventory)


def create_boot(level,
                challenge=None,
                character=None, inventory=None):
    title = 'Мешти'
    image_str = 'boot'
    item_special = ItemSpecial.LEGS.value
    price = 100
    value = 3

    create_armor_items(level=level, title=title, price=price, value=value,
                       image_str=image_str, item_special=item_special,
                       challenge=challenge, character=character,
                       inventory=inventory)


def create_glove(level,
                 challenge=None,
                 character=None, inventory=None):
    title = 'Рукавиці'
    image_str = 'glove'
    item_special = ItemSpecial.HANDS.value
    price = 300
    value = 3

    create_armor_items(level=level, title=title, price=price, value=value,
                       image_str=image_str, item_special=item_special,
                       challenge=challenge, character=character,
                       inventory=inventory)


def create_helmet(level,
                  challenge=None,
                  character=None, inventory=None):
    title = 'Шолом'
    image_str = 'helmet'
    item_special = ItemSpecial.HEAD.value
    price = 300
    value = 3

    create_armor_items(level=level, title=title, price=price, value=value,
                       image_str=image_str, item_special=item_special,
                       challenge=challenge, character=character,
                       inventory=inventory)


def create_iron_shield(level,
                       challenge=None,
                       character=None, inventory=None):
    title = 'Залізний щит'
    image_str = 'iron_shield'
    item_special = ItemSpecial.SHIELD.value
    price = 750
    value = 6

    create_armor_items(level=level, title=title, price=price, value=value,
                       image_str=image_str, item_special=item_special,
                       challenge=challenge, character=character,
                       inventory=inventory)


def create_wooden_shield(level,
                         challenge=None,
                         character=None, inventory=None):
    title = "Дерев'яний щит"
    image_str = 'wooden_shield'
    item_special = ItemSpecial.SHIELD.value
    price = 300
    value = 3

    create_armor_items(level=level, title=title, price=price, value=value,
                       image_str=image_str, item_special=item_special,
                       challenge=challenge, character=character,
                       inventory=inventory)
