from whaleville.core.processors.generator import items_generator
from whaleville.core.enums import ItemType, ItemSpecial


def create_weapon(level, title, price, value, image_str,
                  challenge=None, item_special=None,
                  character=None, inventory=None):
    item_type = ItemType.WEAPON.value
    local_item_special = ItemSpecial.NONE.value
    if item_special:
        local_item_special = item_special
    price, value, title, item_rarity = items_generator.improve_item(
        level, price, value, title)

    items_generator.generate_item(title=title, price=price, value=value,
                                  image_str=image_str, item_type=item_type,
                                  item_special=local_item_special,
                                  item_rarity=item_rarity,
                                  challenge=challenge, character=character,
                                  inventory=inventory)


def create_axe(level,
               challenge=None,
               character=None, inventory=None):
    title = 'Сокира'
    image_str = 'axe'
    price = 80
    value = 4

    create_weapon(level=level, title=title, price=price, value=value,
                  image_str=image_str,
                  challenge=challenge, character=character,
                  inventory=inventory)


def create_bow(level,
               challenge=None,
               character=None, inventory=None):
    title = 'Лук'
    image_str = 'bow'
    item_special = ItemSpecial.BOW.value
    price = 120
    value = 3

    create_weapon(level=level, title=title, price=price, value=value,
                  image_str=image_str, item_special=item_special,
                  challenge=challenge, character=character,
                  inventory=inventory)


def create_club(level,
                challenge=None,
                character=None, inventory=None):
    title = 'Кийок'
    price = 50
    value = 2
    image_str = 'club'

    create_weapon(level=level, title=title, price=price, value=value,
                  image_str=image_str,
                  challenge=challenge, character=character,
                  inventory=inventory)


def create_dagger(level,
                  challenge=None,
                  character=None, inventory=None):
    title = 'Кинджал'
    image_str = 'dagger'
    price = 100
    value = 3

    create_weapon(level=level, title=title, price=price, value=value,
                  image_str=image_str,
                  challenge=challenge, character=character,
                  inventory=inventory)


def create_hammer(level,
                  challenge=None,
                  character=None, inventory=None):
    title = 'Молот'
    image_str = 'hammer'
    price = 500
    value = 7

    create_weapon(level=level, title=title, price=price, value=value,
                  image_str=image_str,
                  challenge=challenge, character=character,
                  inventory=inventory)


def create_lance(level,
                 challenge=None,
                 character=None, inventory=None):
    title = 'Спис'
    image_str = 'lance'
    price = 300
    value = 4

    create_weapon(level=level, title=title, price=price, value=value,
                  image_str=image_str,
                  challenge=challenge, character=character,
                  inventory=inventory)


def create_mace(level,
                challenge=None,
                character=None, inventory=None):
    title = 'Булава'
    image_str = 'mace'
    price = 300
    value = 4

    create_weapon(level=level, title=title, price=price, value=value,
                  image_str=image_str,
                  challenge=challenge, character=character,
                  inventory=inventory)


def create_pickaxe(level,
                   challenge=None,
                   character=None, inventory=None):
    title = 'Кайло'
    image_str = 'pickaxe'
    price = 300
    value = 3

    create_weapon(level=level, title=title, price=price, value=value,
                  image_str=image_str,
                  challenge=challenge, character=character,
                  inventory=inventory)


def create_pitchfork(level,
                     challenge=None,
                     character=None, inventory=None):
    title = 'Тризуб'
    image_str = 'pitchfork'
    price = 700
    value = 6

    create_weapon(level=level, title=title, price=price, value=value,
                  image_str=image_str,
                  challenge=challenge, character=character,
                  inventory=inventory)


def create_wooden_quiver(level,
                         challenge=None,
                         character=None, inventory=None):
    title = "Дерев'яні стріли"
    image_str = 'quiver'
    item_type = ItemType.WEAPON.value
    item_special = ItemSpecial.QUIVER.value
    quantity = 20
    price, value, title, item_rarity = items_generator.improve_item(
        level, 300, 3, title)

    items_generator.generate_item(title=title, price=price, value=value,
                                  image_str=image_str, item_type=item_type,
                                  item_special=item_special,
                                  item_rarity=item_rarity,
                                  challenge=challenge, character=character,
                                  inventory=inventory, quantity=quantity)


def create_copper_quiver(level,
                         challenge=None,
                         character=None, inventory=None):
    title = "Мідні стріли"
    image_str = 'quiver'
    item_type = ItemType.WEAPON.value
    item_special = ItemSpecial.QUIVER.value
    quantity = 20
    price, value, title, item_rarity = items_generator.improve_item(
        level, 500, 4, title)

    items_generator.generate_item(title=title, price=price, value=value,
                                  image_str=image_str, item_type=item_type,
                                  item_special=item_special,
                                  item_rarity=item_rarity,
                                  challenge=challenge, character=character,
                                  inventory=inventory, quantity=quantity)


def create_silver_quiver(level,
                         challenge=None,
                         character=None, inventory=None):
    title = "Срібні стріли"
    image_str = 'quiver'
    item_type = ItemType.WEAPON.value
    item_special = ItemSpecial.QUIVER.value
    quantity = 20
    price, value, title, item_rarity = items_generator.improve_item(
        level, 600, 5, title)

    items_generator.generate_item(title=title, price=price, value=value,
                                  image_str=image_str, item_type=item_type,
                                  item_special=item_special,
                                  item_rarity=item_rarity,
                                  challenge=challenge, character=character,
                                  inventory=inventory, quantity=quantity)


def create_adamant_quiver(level,
                          challenge=None,
                          character=None, inventory=None):
    title = "Адамантові стріли"
    image_str = 'quiver'
    item_type = ItemType.WEAPON.value
    item_special = ItemSpecial.QUIVER.value
    quantity = 20
    price, value, title, item_rarity = items_generator.improve_item(
        level, 1000, 6, title)

    items_generator.generate_item(title=title, price=price, value=value,
                                  image_str=image_str, item_type=item_type,
                                  item_special=item_special,
                                  item_rarity=item_rarity,
                                  challenge=challenge, character=character,
                                  inventory=inventory, quantity=quantity)


def create_rapier(level,
                  challenge=None,
                  character=None, inventory=None):
    title = 'Рапіра'
    image_str = 'rapier'
    price = 500
    value = 5

    create_weapon(level=level, title=title, price=price, value=value,
                  image_str=image_str,
                  challenge=challenge, character=character,
                  inventory=inventory)


def create_stick(level,
                 challenge=None,
                 character=None, inventory=None):
    title = 'Палиця'
    image_str = 'stick'
    price = 200
    value = 2

    create_weapon(level=level, title=title, price=price, value=value,
                  image_str=image_str,
                  challenge=challenge, character=character,
                  inventory=inventory)


def create_sword(level,
                 challenge=None,
                 character=None, inventory=None, title=None):
    title = 'Меч'
    image_str = 'sword'
    price = 400
    value = 4
    if title:
        title = title

    create_weapon(level=level, title=title, price=price, value=value,
                  image_str=image_str,
                  challenge=challenge, character=character,
                  inventory=inventory)
