from whaleville.core.processors.generator import items_generator
from whaleville.core.enums import ItemType, ItemSpecial, ItemRarity


def create_potion_item(level, title, image_str, price, value,
                       item_special, challenge=None,
                       character=None, inventory=None):
    item_type = ItemType.POTION.value
    item_rarity = ItemRarity.REGULAR.value

    items_generator.generate_item(title=title, price=price, value=value,
                                  image_str=image_str, item_type=item_type,
                                  item_special=item_special,
                                  item_rarity=item_rarity,
                                  challenge=challenge, character=character,
                                  inventory=inventory)


def create_small_health_potion(level,
                               challenge=None,
                               character=None, inventory=None):
    title = 'Мале Зілля Зцілення'
    price = 200
    value = 5
    image_str = 'small_potion'
    item_special = ItemSpecial.HEALTH.value

    create_potion_item(level=level, title=title, price=price, value=value,
                       image_str=image_str,
                       item_special=item_special,
                       challenge=challenge, character=character,
                       inventory=inventory)


def create_middle_health_potion(level,
                                challenge=None,
                                character=None, inventory=None):
    title = 'Середнє Зілля Зцілення'
    price = 350
    value = 8
    image_str = 'middle_potion'
    item_special = ItemSpecial.HEALTH.value

    create_potion_item(level=level, title=title, price=price, value=value,
                       image_str=image_str,
                       item_special=item_special,
                       challenge=challenge, character=character,
                       inventory=inventory)


def create_big_health_potion(level,
                             challenge=None,
                             character=None, inventory=None):
    title = 'Велике Зілля Зцілення'
    price = 600
    value = 14
    image_str = 'big_potion'
    item_special = ItemSpecial.HEALTH.value

    create_potion_item(level=level, title=title, price=price, value=value,
                       image_str=image_str,
                       item_special=item_special,
                       challenge=challenge, character=character,
                       inventory=inventory)


def create_small_restoration_potion(level,
                                    challenge=None,
                                    character=None, inventory=None):
    title = 'Мале Зілля Відновлення'
    price = 150
    value = 15
    image_str = 'small_potion'
    item_special = ItemSpecial.EXHAUSTION.value

    create_potion_item(level=level, title=title, price=price, value=value,
                       image_str=image_str,
                       item_special=item_special,
                       challenge=challenge, character=character,
                       inventory=inventory)


def create_middle_restoration_potion(level,
                                     challenge=None,
                                     character=None, inventory=None):
    title = 'Середнє Зілля Відновлення'
    price = 250
    value = 25
    image_str = 'middle_potion'
    item_special = ItemSpecial.EXHAUSTION.value

    create_potion_item(level=level, title=title, price=price, value=value,
                       image_str=image_str,
                       item_special=item_special,
                       challenge=challenge, character=character,
                       inventory=inventory)


def create_big_restoration_potion(level,
                                  challenge=None,
                                  character=None, inventory=None):
    title = 'Велике Зілля Відновлення'
    price = 400
    value = 35
    image_str = 'big_potion'
    item_special = ItemSpecial.EXHAUSTION.value

    create_potion_item(level=level, title=title, price=price, value=value,
                       image_str=image_str,
                       item_special=item_special,
                       challenge=challenge, character=character,
                       inventory=inventory)


# TODO: Work out new item type for healing items like bandage or herbs
# def create_herbs(level,
#                  challenge=None,
#                  character=None, inventory=None):
#     image_str = 'herbs'
#     item_special = ItemSpecial.HEALTH.value
#     price = 25
#     value = 1
#     title = 'Цілющі трави'

#     create_potion_item(title=title, price=price, value=value,
#                        image_str=image_str,
#                        item_special=item_special,
#                        challenge=challenge, character=character,
#                        inventory=inventory)

# def create_bandage(level,
#                    challenge=None,
#                    character=None, inventory=None):
#     title = 'Бинт'
#     price = 50
#     value = 4
#     image_str = 'bandage'
#     item_special = ItemSpecial.HEALTH.value

#     create_potion_item(title=title, price=price, value=value,
#                        image_str=image_str,
#                        item_special=item_special,
#                        challenge=challenge, character=character,
#                        inventory=inventory)
