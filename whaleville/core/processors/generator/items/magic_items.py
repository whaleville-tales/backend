from whaleville.core.processors.generator import items_generator
from whaleville.core.enums import ItemType


def create_magic_item(level, title, image_str, price, value,
                      challenge=None,
                      character=None, inventory=None):
    item_type = ItemType.SUPPLY.value
    item_special = items_generator.get_supply_type_for_magic_items().value
    price, value, title, item_rarity = items_generator.improve_item(
        level, price, value, title)

    items_generator.generate_item(title=title, price=price, value=value,
                                  image_str=image_str, item_type=item_type,
                                  item_special=item_special,
                                  item_rarity=item_rarity,
                                  challenge=challenge, character=character,
                                  inventory=inventory)


def create_amulet(level,
                  challenge=None,
                  character=None, inventory=None):
    title = 'Амулет'
    image_str = 'amulet'
    price = 750
    value = 5

    create_magic_item(level=level, title=title, price=price, value=value,
                      image_str=image_str,
                      challenge=challenge, character=character,
                      inventory=inventory)


def create_ring(level,
                challenge=None,
                character=None, inventory=None):
    title = 'Перстень'
    image_str = 'ring'
    price = 500
    value = 3

    create_magic_item(level=level, title=title, price=price, value=value,
                      image_str=image_str,
                      challenge=challenge, character=character,
                      inventory=inventory)
