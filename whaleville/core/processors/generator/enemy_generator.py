import random
from config.settings.base import APPS_DIR, LOOT_ENEMY_CHANCE
from whaleville.core.processors import attributes
from whaleville.core.processors.generator import (
    character_generator, items_generator)
from whaleville.core.enums import CharacterType, CharacterClassType
from whaleville.core.models.character.character import Character
from whaleville.core.processors.generator.items import (
    weapon_items, armor_items)


def update_enemies_level(game):
    enemies = Character.objects.filter(
        game=game, character_type=CharacterType.ENEMY.value)
    party = Character.objects.filter(
        game=game, character_type=CharacterType.PARTY.value)

    # get min party level
    experience_list = list()
    for character in party:
        experience_list.append(character.attributes.experience)
    min_experience = min(experience_list)
    for character in enemies:
        enemies_to_avoid = ["Велетенський огрядний гриб",
                            "Старий відьмак", "Гігантський павук"]
        if character not in enemies_to_avoid:
            initial_level = character.attributes.level.level
            character.attributes.experience = min_experience * 0.65
            character.attributes.save()
            if character.attributes.level.level != initial_level:
                times = character.attributes.level.level - initial_level
                attributes.levelup(character, times)
                character.attributes.current_hp = character.attributes.total_hp
                character.attributes.save()


def handle_loot(enemy):
    if random.randrange(100) < LOOT_ENEMY_CHANCE:
        items_generator.generate_loot_for_character(enemy)


def generate_bandit(challenge):
    bandit = character_generator.generate_character(
        game=challenge.quest.game, character_type=CharacterType.ENEMY.value,
        experience=0, class_type=CharacterClassType.WARRIOR.value)
    handle_loot(bandit)
    bandit.name = "{0} бандит".format(random.choice(open(
        '{0}/core/processors/generator/content/enemies/bandit.txt'.format(
            APPS_DIR)).readlines()).rstrip())
    bandit.challenge = challenge
    bandit.save()


def generate_smuggler(challenge):
    smuggler = character_generator.generate_character(
        game=challenge.quest.game, character_type=CharacterType.ENEMY.value,
        experience=0, class_type=CharacterClassType.WARRIOR.value)
    handle_loot(smuggler)
    smuggler.name = "{0} контрабандист".format(random.choice(open(
        '{0}/core/processors/generator/content/enemies/bandit.txt'.format(
            APPS_DIR)).readlines()).rstrip())
    smuggler.challenge = challenge
    smuggler.save()


def generate_infected(challenge):
    infected = character_generator.generate_character(
        game=challenge.quest.game, character_type=CharacterType.ENEMY.value,
        experience=500)
    handle_loot(infected)
    infected.name = random.choice(open(
        '{0}/core/processors/generator/content/enemies/infected.txt'.format(
            APPS_DIR)).readlines()).rstrip()
    infected.challenge = challenge
    infected.save()


def generate_cultist(challenge):
    cultist = character_generator.generate_character(
        game=challenge.quest.game, character_type=CharacterType.ENEMY.value,
        experience=500)
    handle_loot(cultist)
    cultist.name = "Культист {0}".format(random.choice(open(
        '{0}/core/processors/generator/content/enemies/cultist.txt'.format(
            APPS_DIR)).readlines()).rstrip())
    cultist.challenge = challenge
    cultist.save()


def generate_sea_monster(challenge):
    sea_monster = character_generator.generate_character(
        game=challenge.quest.game, character_type=CharacterType.ENEMY.value,
        experience=500, class_type=CharacterClassType.ROGUE.value)
    handle_loot(sea_monster)
    sea_monster.name = random.choice(open(
        '{0}/core/processors/generator/content/enemies/sea_monster.txt'.format(  # noqa
            APPS_DIR)).readlines()).rstrip()
    sea_monster.challenge = challenge
    sea_monster.save()
    sea_monster.skills.all().delete()


def generate_giant_spider(challenge):
    giant_spider = character_generator.generate_character(
        game=challenge.quest.game, character_type=CharacterType.ENEMY.value,
        experience=15000, class_type=CharacterClassType.ROGUE.value)
    giant_spider.name = "Гігантський павук"
    giant_spider.challenge = challenge
    giant_spider.save()
    # generate loot
    weapon_items.create_sword(level=5,
                              character=giant_spider,
                              title="Жало Гігантського Павука")
    giant_spider.skills.all().delete()


def generate_witcher(challenge):
    witcher = character_generator.generate_character(
        game=challenge.quest.game, character_type=CharacterType.ENEMY.value,
        experience=15000, class_type=CharacterClassType.WIZARD.value)
    witcher.name = "Старий відьмак"
    witcher.challenge = challenge
    witcher.save()
    # generate loot
    armor_items.create_armor(level=5,
                             character=witcher,
                             title="Обладунки Старого Відьмака")


def generate_fungus(challenge):
    fungus = character_generator.generate_character(
        game=challenge.quest.game, character_type=CharacterType.ENEMY.value,
        experience=45000, class_type=CharacterClassType.WARRIOR.value)
    fungus.name = "Велетенський огрядний гриб"
    fungus.challenge = challenge
    fungus.save()

    fungus.skills.all().delete()
