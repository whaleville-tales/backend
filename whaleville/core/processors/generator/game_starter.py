from whaleville.core.models.game import Game
from whaleville.core.models.character.character import Character
from whaleville.core.enums import CharacterType
from whaleville.core.processors.generator import (quest_generator,
                                                  character_generator,
                                                  items_generator,
                                                  guild_generator)


def start_game(user):
    # create game
    game = Game()
    game.user = user
    game.save()
    # create quests chains
    quest_generator.create_quest_chains(game)
    # create quests
    quest_generator.create_quests(game)
    quest_generator.reveal_initial_quest(game)
    # create market and shared inventory
    items_generator.create_market(game)
    items_generator.create_shared_inventory(game)
    # create party
    character_generator.create_party(game)
    # create guild
    party = Character.objects.filter(
        game=game,
        character_type=CharacterType.PARTY.value)
    guild_generator.generate_guild_members(party)
    return game


def reset_game(user):
    game = user.game
    game.delete()
    return start_game(user)
