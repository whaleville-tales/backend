import random
from config.settings.base import (
    MIN_CHARACTERS_TO_GENERATE, MAX_CHARACTERS_TO_GENERATE)
from whaleville.core.enums import CharacterType, QuestType
from whaleville.core.models.character.character import Character
from whaleville.core.processors.generator import character_generator


def generate_guild_members(party):
    # get max level in the party
    experience_list = list()
    number_of_characters_to_generate = random.randrange(
        MIN_CHARACTERS_TO_GENERATE, MAX_CHARACTERS_TO_GENERATE + 1)
    game = party.first().game
    quests_faced = len(
        game.quests.exclude(
            quest_type=QuestType.HIDDEN.value))
    for character in party:
        experience_list.append(character.attributes.experience)
    max_experience = max(experience_list)
    # clear all guild characters
    Character.objects.filter(game=game,
                             character_type=CharacterType.GUILD.value).delete()

    for tick in list(range(number_of_characters_to_generate)):
        character_experience = max_experience + random.randrange(500)
        price = 500 + random.randrange(500) * quests_faced
        character_generator.generate_character(
            game=game, character_type=CharacterType.GUILD.value,
            price=price, experience=character_experience)
