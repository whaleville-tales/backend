import csv
from config.settings.base import APPS_DIR
from whaleville.core.models.quest.quest import Quest
from whaleville.core.models.quest.quest_data import QuestData
from whaleville.core.models.quest.quest_chain import QuestChain
from whaleville.core.models.quest.challenge import Challenge
from whaleville.core.models.quest.challenge_data import ChallengeData
from whaleville.core.enums import QuestType, ActionLogType, ChallengeEnemy
from whaleville.core.processors import exploration
from whaleville.core.processors.tracking import log_action
from whaleville.core.processors.generator import (
    guild_generator, items_generator, event_generator, enemy_generator)


def create_challenges_data(quest):
    path = '{0}/core/processors/generator/content/challenges/{1}.csv'.format(
        APPS_DIR, quest.title)
    with open(path, newline='') as csvfile:
        filereader = csv.reader(csvfile)
        for row in filereader:
            iterable_row = iter(row)
            challenge = ChallengeData()
            challenge.quest_title = quest.title
            challenge.description = next(iterable_row)
            challenge.success_challenge_description = next(iterable_row)
            challenge.failed_challenge_description = next(iterable_row)
            challenge.challenge_type = next(iterable_row)
            challenge.difficulty = next(iterable_row)
            challenge.damage_on_fail = next(iterable_row)
            challenge.target_type = next(iterable_row)
            challenge.skip_next = next(iterable_row)
            if next(iterable_row):
                challenge.is_lootable = True
            challenge.enemies_type = next(iterable_row)
            quantity = next(iterable_row)
            if quantity:
                challenge.enemies_quantity = quantity
            challenge.save()


def create_challenges(quest):
    path = '{0}/core/processors/generator/content/challenges/{1}.csv'.format(
        APPS_DIR, quest.title)
    with open(path, newline='') as csvfile:
        filereader = csv.reader(csvfile)
        for row in filereader:
            latest_challenge = Challenge.objects.filter(quest=quest).last()
            if latest_challenge:
                data_id = latest_challenge.data_id + 1
            else:
                data_id = ChallengeData.objects.filter(
                    quest_title=quest.title).first().pk
            challenge = Challenge()
            challenge.quest = quest
            challenge.data_id = data_id
            challenge.save()

            challenge_data = ChallengeData.objects.get(pk=challenge.data_id)

            if challenge_data.is_lootable:
                # create items for challenges
                items_generator.generate_loot_for_challenge(challenge)

            if challenge_data.enemies_type != ChallengeEnemy.NONE.value:
                # create enemies for challenges
                for enemy in list(range(0, challenge_data.enemies_quantity)):
                    if challenge_data.enemies_type == ChallengeEnemy.BANDIT.value:  # noqa
                        enemy_generator.generate_bandit(challenge)
                    elif challenge_data.enemies_type == ChallengeEnemy.SMUGGLER.value:  # noqa
                        enemy_generator.generate_smuggler(challenge)
                    elif challenge_data.enemies_type == ChallengeEnemy.CULTIST.value:  # noqa
                        enemy_generator.generate_cultist(challenge)
                    elif challenge_data.enemies_type == ChallengeEnemy.INFECTED_CREATURE.value:  # noqa
                        enemy_generator.generate_infected(challenge)
                    elif challenge_data.enemies_type == ChallengeEnemy.SEA_MONSTER.value:  # noqa
                        enemy_generator.generate_sea_monster(challenge)
                    elif challenge_data.enemies_type == ChallengeEnemy.GIANT_SPIDER.value:  # noqa
                        enemy_generator.generate_giant_spider(challenge)
                    elif challenge_data.enemies_type == ChallengeEnemy.WITCHER.value:  # noqa
                        enemy_generator.generate_witcher(challenge)
                    elif challenge_data.enemies_type == ChallengeEnemy.FUNGUS.value:  # noqa
                        enemy_generator.generate_fungus(challenge)


def create_quests_data():
    path = '{0}/core/processors/generator/content/quests.csv'.format(APPS_DIR)
    with open(path, newline='') as csvfile:
        filereader = csv.reader(csvfile)
        for row in filereader:
            iterable_row = iter(row)
            quest = QuestData()
            quest.title = next(iterable_row)
            quest.description = next(iterable_row)
            quest.report = next(iterable_row)
            quest.experience_value = next(iterable_row)
            quest.reward_value = next(iterable_row)
            quest.quest_chain = next(iterable_row)
            quest.image_str = next(iterable_row)
            quests_to_reveal = list(iterable_row)
            quest.reveal_quests = quests_to_reveal
            quest.save()
            # create challenges data
            create_challenges_data(quest)


def create_quests(game):
    path = '{0}/core/processors/generator/content/quests.csv'.format(APPS_DIR)
    with open(path, newline='') as csvfile:
        filereader = csv.reader(csvfile)
        data_id = QuestData.objects.first().pk
        for row in filereader:
            quest_data = QuestData.objects.get(pk=data_id)
            quest = Quest()
            quest.quest_chain = QuestChain.objects.get(
                game=game,
                title=quest_data.quest_chain)
            quest.game = game
            quest.data_id = data_id
            quest.quest_type = QuestType.HIDDEN.value
            quest.save()
            data_id += 1
            # create challenges
            create_challenges(quest)


def reveal_initial_quest(game):
    # set first quest as NEW in order to find it on game beginning
    first_quest = Quest.objects.filter(game=game).first()
    first_quest.quest_type = QuestType.NEW.value
    first_quest.save()


def create_quest_chains(game):
    names = [
        'Неспокій в Китовому',
        'Загроза з глибин',
        'Загадка запліснявілих шахт']
    for name in names:
        quest_chain = QuestChain()
        quest_chain.game = game
        quest_chain.title = name
        quest_chain.save()


def complete_quest(party, quest, game):
    quest.quest_type = QuestType.COMPLETED.value
    quest.save()
    exploration.get_quest_reward(quest, game)
    guild_generator.generate_guild_members(party)
    # update items on market
    items_generator.update_market_items(game)
    text = 'Завдання {0} виконано'.format(quest.title)
    log_action(quest, text, ActionLogType.SUCCESS.value)
    event_generator.handle_quest_complete_event(quest, game)
    # update enemies level
    enemy_generator.update_enemies_level(game)
