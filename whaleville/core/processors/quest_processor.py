import time
import threading
from config.settings.base import ACTION_DELAY

from whaleville.core.enums import QuestType, ActionLogType
from whaleville.core.models.quest.quest import Quest
from whaleville.core.models.game import Game
from whaleville.core.models.character.character import Character
from whaleville.core.enums import (
    CharacterType, ChallengeCheck, SkipNext)
from whaleville.core.processors import combat
from whaleville.core.processors import exploration
from whaleville.core.processors.tracking import log_action
from whaleville.core.processors.generator import quest_generator
from whaleville.core.processors import restore_processor


def thread_quest_process(quest_uuid, game_id):
    x = threading.Thread(
        target=process_quest,
        args=(
            quest_uuid,
            game_id),
        daemon=True)
    x.start()


def process_quest(quest_uuid, game_id):
    quest = Quest.objects.get(uuid=quest_uuid)
    game = Game.objects.get(pk=game_id)
    party = game.characters.filter(
        character_type=CharacterType.PARTY.value)
    challenges = quest.challenges.all()
    skip_next = 0
    for challenge in challenges:
        time.sleep(ACTION_DELAY)
        if skip_next > 0:
            skip_next -= 1
            continue
        else:
            # describe location
            text = '{0}'.format(challenge.description)
            log_action(quest, text)
            enemies = challenge.enemies.all()
            if enemies:
                # describe enemies
                enemy_names = combat.get_enemy_names(enemies)
                text = 'Перед вами {0}'.format(enemy_names)
                log_action(quest, text)
                # start fight
                queue = combat.form_combat_queue(list(party), list(enemies))
                while combat.characters_are_alive(
                        enemies) and combat.characters_are_alive(
                        party):
                    for character in queue:
                        # refresh data
                        character = Character.objects.get(pk=character.pk)
                        quest = Quest.objects.get(uuid=quest_uuid)
                        if quest.quest_type == QuestType.FAILED.value:
                            break
                        if character.character_type != CharacterType.DEAD.value:  # noqa
                            if character.character_type == CharacterType.ENEMY.value:  # noqa
                                target = combat.get_target(party)
                                if not target:
                                    continue
                                opponents = party
                                allies = enemies
                            else:
                                target = combat.get_target(enemies)
                                if not target:
                                    continue
                                opponents = enemies
                                allies = party
                            combat.fight(
                                character, target, opponents, allies, quest)
                if not combat.characters_are_alive(enemies):
                    party = game.characters.filter(
                        character_type=CharacterType.PARTY.value)
                    for enemy in enemies:
                        exploration.get_loot(
                            enemy.loot.all(), party, quest, enemy.name)
            else:
                # non-combat challenge...
                if challenge.challenge_type != ChallengeCheck.NOTHING.value:
                    chosen = exploration.get_character_for_challenge(
                        challenge.challenge_type, party)
                    text = '{0} намагається впоратися з побаченим'.format(
                        chosen.name)
                    log_action(quest, text, ActionLogType.CHECK.value)
                    time.sleep(ACTION_DELAY)
                    challenge_result = exploration.check_challenge(
                        challenge.challenge_type, challenge.difficulty,
                        chosen)
                    if challenge_result:
                        if challenge.skip_next == SkipNext.ONEONSUCCESS.value:
                            skip_next = 1
                        if challenge.skip_next == SkipNext.TWOONSUCCESS.value:
                            skip_next = 2
                        text = '{0} успішно дає раду з випробуванням!'.format(
                            chosen.name)
                        log_action(quest, text, ActionLogType.SUCCESS.value)
                        text = '{0}'.format(
                            challenge.success_challenge_description)
                        log_action(quest, text)
                    else:
                        if challenge.skip_next == SkipNext.ONEONFAILURE.value:
                            skip_next = 1
                        if challenge.skip_next == SkipNext.TWOONFAILURE.value:
                            skip_next = 2
                        exploration.handle_challenge_fail(
                            challenge, chosen, party)

                exploration.get_loot(challenge.loot.all(), party, quest)

            challenge.done = True
            challenge.save()
    quest_generator.complete_quest(party, quest, game)
    time.sleep(ACTION_DELAY)


def fail_quest(quest, target_name):
    party = quest.game.characters.filter(
        character_type=CharacterType.PARTY.value)
    quest.quest_type = QuestType.FAILED.value
    text = 'На жаль, ви не розрахували свої сили. {0} виявилося надто небезпечним завданням. {1} лишившись наодинці з ворогом та з численними пошкодженнями дивом вибираєтеся назад у Китове'.format(  # noqa
        quest.title, target_name)
    log_action(
        quest, text, ActionLogType.FAIL.value)
    quest.save()
    # start restoring characters
    for character in party:
        restore_processor.thread_restore_character_process(character)
