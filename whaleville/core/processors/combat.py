import random
import time
from config.settings.base import (
    ACTION_DELAY, SKILL_USE_CHANCE, HIT_OCCURS_CHANCE,
    HP_PERCENTS_TO_USE_POTIONS, SKILL_USE_EXHAUSTION_COST,
    HIT_WIZARD_CHANCE, HIT_ROGUE_CHANCE, RESET_ARMOR_BOOST_ON_TURN_START,
    EXHAUSTION_ON_HIT_BASE)
from whaleville.core.models.character.character import Character
from whaleville.core.models.character.death import Death
from whaleville.core.processors.tracking import log_action
from whaleville.core.processors.generator import event_generator
from whaleville.core.processors import quest_processor
from whaleville.core.enums import (
    CharacterType, CharacterClassType, ItemType, ItemSpecial,
    SkillType, TargetType, QuestType, ActionLogType)


def get_death_reason(target, character=None, skill_title=None):
    reason = 'Смерть знайшла {0}'.format(target.name)
    current_quest = target.game.quests.get(
        quest_type=QuestType.ACTIVE.value)
    death_desc = ''
    if character:
        death_desc = 'могуть'
        weapon = character.attributes.weapon
        if weapon:
            death_desc = weapon.title
        if skill_title:
            death_desc = skill_title
        death_desc = '{0} та його {1}'.format(
            character.name, death_desc)
    if target.sex_type == 'Male':
        sex_desc = 'нього'
    else:
        sex_desc = 'неї'

    reason = '{0} помирає намагаючись {1}. {2} стає для {3} знаряддям Смерті. Спочивай у мирі, {0}'.format(  # noqa
        target.name, current_quest.title.lower(), death_desc, sex_desc)
    return reason


def hit(target, damage, quest, character=None, skill_title=None):
    target.attributes.current_hp -= damage
    if target.attributes.current_hp < 1:
        if target.character_type == CharacterType.PARTY.value:
            if len(
                target.game.characters.filter(
                    character_type=CharacterType.PARTY.value)) == 1:
                # lastest party character flees instead of dying
                quest_processor.fail_quest(quest, target.name)
            # handle party member death
            target.death = Death()
            death_reason = get_death_reason(
                target, character, skill_title)
            target.death.death_reason = death_reason
            target.death.save()
            event_generator.handle_death_event(
                target.game, target, death_reason)

        target.character_type = CharacterType.DEAD.value
        target.save()
        text = '{0} помирає'.format(target.name, damage)
        log_action(quest, text, ActionLogType.DEATH.value)
    else:
        text = '{0} почувається на {1}/{2}'.format(
            target.name, target.attributes.current_hp,
            target.attributes.total_hp)
        log_action(quest, text)
    target.attributes.save()
    return True


def hit_exhaustion(character, quest):
    # based on const + intelligence
    characteristics = character.characteristics
    character_bonus = characteristics.constitution +\
        characteristics.intelligence
    character.attributes.current_exhaustion += round(
        (EXHAUSTION_ON_HIT_BASE / character_bonus) * 10)
    if character.attributes.current_exhaustion > character.attributes.total_exhaustion:  # noqa
        character.attributes.current_exhaustion = character.attributes.total_exhaustion  # noqa
        text = '{0} почувається вкрай виснажено'.format(character.name)
        log_action(quest, text)
        # try to recover exhaustion level
        try_to_handle_exhaustion(character, quest)
    character.attributes.save()


def form_combat_queue(party, enemies):
    # sort by perception -> dexterity
    queue = party
    queue.extend(enemies)
    queue = sorted(
        queue, key=lambda x: (
            x.characteristics.perception, x.characteristics.dexterity),
        reverse=True)
    return queue


def does_hit_occures(character, skill_effect=None):
    base_percent = HIT_OCCURS_CHANCE
    items_value = 0
    additional_effect = 0
    if skill_effect:
        additional_effect = skill_effect
    items = character.inventory.first().items.filter(
        item_type=ItemType.SUPPLY.value,
        item_special=ItemSpecial.ACCURACY.value)
    if items:
        for item in items:
            items_value += item.value

    total_percent = base_percent + additional_effect + \
        items_value + character.characteristics.dexterity

    return random.randrange(100) < total_percent


def calculate_damage(character, skill_effect=None):
    """
    Обрахунок пошкоджень
    """
    items_value = 0
    additional_effect = 0
    exhaustion_effect = 0
    if skill_effect:
        additional_effect = skill_effect
    items = character.inventory.first().items.filter(
        item_type=ItemType.SUPPLY.value, item_special=ItemSpecial.WEAPON.value)
    if items:
        for item in items:
            items_value += item.value

    total_bonus = round((additional_effect + items_value) / 5)
    # reduce damage on exhaustion
    if character.attributes.current_exhaustion == character.attributes.total_exhaustion:  # noqa
        exhaustion_effect = character.attributes.damage / 3
    return round(character.attributes.damage +
                 total_bonus - exhaustion_effect)


def calculate_damage_absorbtion(damage, target, skill_effect=None):
    """
    поглинання пошкоджень
    """
    items_value = 0
    additional_effect = 0
    absorbed = 0
    if skill_effect:
        additional_effect = skill_effect

    items = target.inventory.first().items.filter(
        item_type=ItemType.SUPPLY.value, item_special=ItemSpecial.ARMOR.value)
    if items:
        for item in items:
            items_value += item.value

    total_bonus = (target.attributes.armor +
                   items_value + additional_effect) * 2
    if total_bonus > 0:
        absorbed = damage / 100 * total_bonus
    damage_left = damage - absorbed
    if damage_left < 0:
        damage_left = 0
    return round(damage_left), round(absorbed)


def characters_are_alive(characters):
    for character in characters:
        # refresh data
        character = Character.objects.get(pk=character.pk)
        if character.character_type != CharacterType.DEAD.value:
            return True
    return False


def get_target(target_list):
    alive_targets = target_list.exclude(
        character_type=CharacterType.DEAD.value)
    if alive_targets:
        target = alive_targets[0]
        destiny_value = random.randrange(100)
        if destiny_value < HIT_WIZARD_CHANCE:
            wizard_list = alive_targets.filter(
                class_type=CharacterClassType.WIZARD.value)
            if wizard_list:
                target = wizard_list[0]
        elif destiny_value < HIT_ROGUE_CHANCE:
            rogue_list = alive_targets.filter(
                class_type=CharacterClassType.ROGUE.value)
            if rogue_list:
                target = rogue_list[0]
        else:
            warrior_list = alive_targets.filter(
                class_type=CharacterClassType.WARRIOR.value)
            if warrior_list:
                target = warrior_list[0]

        return target
    return False


def healing_phase(character, quest):
    # heal itself if hp is low
    total_hp = character.attributes.total_hp
    if (character.attributes.current_hp / total_hp) * \
            100 < HP_PERCENTS_TO_USE_POTIONS:
        health_potions = character.inventory.first().items.filter(
            item_type=ItemType.POTION.value,
            item_special=ItemSpecial.HEALTH.value)
        food_items = character.inventory.first().items.filter(
            item_type=ItemType.SUPPLY.value,
            item_special=ItemSpecial.FOOD.value)
        if health_potions:
            # use potion
            potion = health_potions[0]
            character.attributes.current_hp += potion.value
            character.attributes.save()
            text = '{0} використовує {1} і відновлює рівень здоровʼя до {2}/{3}'.format(  # noqa
                    character.name, potion.title,
                    character.attributes.current_hp, total_hp)
            log_action(quest, text, ActionLogType.ITEM.value)
            potion.delete()
        elif food_items:
            # use food
            food = food_items[0]
            character.attributes.current_hp += food.value
            character.attributes.save()
            text = '{0} вживає {1} і відновлює рівень здоровʼя до {2}/{3}'.format(  # noqa
                character.name,
                food.title, character.attributes.current_hp, total_hp)
            log_action(quest, text, ActionLogType.ITEM.value)
            food.delete()


def try_to_handle_exhaustion(character, quest):
    current_exhaustion = character.attributes.current_exhaustion
    total_exhaustion = character.attributes.total_exhaustion
    recover_potions = character.inventory.first().items.filter(
        item_type=ItemType.POTION.value,
        item_special=ItemSpecial.EXHAUSTION.value)

    if recover_potions:
        # use potion
        potion = recover_potions[0]
        current_exhaustion -= potion.value
        character.attributes.current_exhaustion = current_exhaustion
        character.attributes.save()
        text = '{0} використовує {1} і виснаження відступає до рівня {2}/{3}'.format(  # noqa
                character.name,
                potion.title, current_exhaustion, total_exhaustion)
        log_action(quest, text, ActionLogType.ITEM.value)
        potion.delete()


def update_quiver(weapon, quiver, character_name, quest):
    if weapon and quiver:
        if weapon.item_special == ItemSpecial.BOW.value:
            if quiver.quantity == 1:
                # last arrow was used
                quiver.delete()
                text = '{0} використовує останню стрілу з колчана.{1} скінчилися'.format(  # noqa
                        character_name, quiver.title)
                log_action(quest, text, ActionLogType.ITEM.value)
            else:
                quiver.quantity -= 1
                quiver.save()


def get_enemy_names(enemies):
    enemy_names = ''
    for enemy in enemies:
        if enemy.uuid == enemies[len(enemies) - 1].uuid:
            if len(enemies) == 1:
                enemy_names += '{0}'.format(enemy.name)
            else:
                enemy_names = enemy_names[:-2]
                enemy_names += ' та {0}'.format(enemy.name)
        else:
            enemy_names += '{0}, '.format(enemy.name)
    return enemy_names


def skills_phase(character, target, opponents, allies, quest):
    if RESET_ARMOR_BOOST_ON_TURN_START:
        # remove armor boost
        if character.attributes.armor_boost > 0:
            character.attributes.armor_boost = 0
            text = '{0} втрачає додатковий захист'.format(
                character.name)
            log_action(quest, text, ActionLogType.SKILL.value)
            character.attributes.save()
    # check if we'll use the skill this turn at all
    if random.randrange(100) < SKILL_USE_CHANCE:
        if (character.attributes.current_exhaustion + SKILL_USE_EXHAUSTION_COST) < character.attributes.total_exhaustion:  # noqa
            skills = character.skills.exclude(is_hidden=True)
            if skills:
                skill = random.choice(skills)
                text = '{0} використовує {1}'.format(  # noqa
                                character.name, skill.title)
                log_action(quest, text, ActionLogType.SKILL.value)
                if skill.description:
                    if '{0}' in skill.description:
                        log_action(
                            quest, skill.description.format(
                                character.name), ActionLogType.SKILL.value)
                    else:
                        log_action(
                            quest, skill.description,
                            ActionLogType.SKILL.value)
                # handle skill type and target type
                # handle DAMAGE type
                if skill.skill_type == SkillType.DAMAGE.value:
                    if skill.target_type == TargetType.CURRENT.value:
                        text = '{0} отримує {1} пошкоджень'.format(  # noqa
                                target.name, skill.value)
                        log_action(quest, text, ActionLogType.DAMAGE.value)
                        hit(target, skill.value, quest, character, skill.title)
                    elif skill.target_type == TargetType.GROUP.value:
                        text = 'Всі вороги отримують по {0} пошкоджень'.format(  # noqa
                                skill.value)
                        log_action(quest, text, ActionLogType.DAMAGE.value)
                        for target in opponents:
                            hit(target, skill.value, quest,
                                character, skill.title)
                # handle ARMOR type
                if skill.skill_type == SkillType.ARMOR.value:
                    if skill.target_type == TargetType.SELF.value:
                        character.attributes.armor_boost += skill.value
                        text = '{0} збільшує рівень захисту на {1}'.format(  # noqa
                                character.name,
                                character.attributes.armor_boost)
                        log_action(quest, text)
                    elif skill.target_type == TargetType.GROUP.value:
                        text = 'Весь гурт збільшує рівень захисту на {0}'.format(  # noqa
                                skill.value)
                        log_action(quest, text)
                        for ally in allies:
                            ally.attributes.armor_boost += skill.value
                            ally.attributes.save()
                # handle HEAL type
                if skill.skill_type == SkillType.HEAL.value:
                    if skill.target_type == TargetType.SELF.value:
                        character.attributes.current_hp += skill.value
                        text = '{0} збільшує рівень здоровʼя до {1}/{2}'.format(  # noqa
                                character.name,
                                character.attributes.current_hp,
                                character.attributes.total_hp)
                        log_action(quest, text)
                    elif skill.target_type == TargetType.GROUP.value:
                        text = 'Весь гурт збільшує рівень здоровʼя на {0}'.format(  # noqa
                                skill.value)
                        log_action(quest, text)
                        for ally in allies:
                            ally.attributes.current_hp += skill.value
                            ally.attributes.save()
                # handle RESTORE type
                if skill.skill_type == SkillType.RESTORE.value:
                    if skill.target_type == TargetType.SELF.value:
                        character.attributes.current_exhaustion -= skill.value
                        if character.attributes.current_exhaustion < 0:
                            character.attributes.current_exhaustion = 0
                        text = '{0} відчуває як виснаження відступає до рівня {1}/{2}'.format(  # noqa
                            character.name,
                            character.attributes.current_exhaustion,
                            character.attributes.total_exhaustion)
                        log_action(quest, text)
                    elif skill.target_type == TargetType.GROUP.value:
                        text = 'Весь гурт зменшує виснаження на {0}'.format(  # noqa
                            skill.value)
                        log_action(quest, text)
                        for ally in allies:
                            ally.attributes.current_exhaustion -= skill.value
                            if ally.attributes.current_exhaustion < 0:
                                ally.attributes.current_exhaustion = 0
                            ally.attributes.save()
            character.attributes.current_exhaustion += SKILL_USE_EXHAUSTION_COST  # noqa
            character.attributes.save()


def fight(character, target, opponents, allies, quest):
    # firstly check for skills options
    skills_phase(character, target, opponents, allies, quest)
    # then check for healing
    healing_phase(character, quest)
    if not target.character_type == CharacterType.DEAD.value:
        time.sleep(ACTION_DELAY)
        weapon_description = ''
        if character.attributes.weapon:
            weapon_description = ' використовуючи {0}'.format(
                character.attributes.weapon.title)
            if character.attributes.weapon.item_special == ItemSpecial.BOW.value and character.attributes.quiver:  # noqa
                weapon_description += ' та {0}'.format(
                    character.attributes.quiver.title)
        text = '{0} атакує {1}{2}'.format(
            character.name, target.name, weapon_description)
        log_action(quest, text, ActionLogType.DAMAGE.value)
        # decrease arrows quantity, drop empty quiver
        update_quiver(
            character.attributes.weapon,
            character.attributes.quiver,
            character.name, quest)
        time.sleep(ACTION_DELAY)
        # character exhausts on each action
        hit_exhaustion(character, quest)
        # check does hit occurs
        hit_occurs = does_hit_occures(character)
        if hit_occurs:
            damage = calculate_damage(character)
            text = '{0} наносить {1} пошкоджень'.format(
                character.name, damage)
            log_action(quest, text, ActionLogType.DAMAGE.value)
            damage_left, absorbed = \
                calculate_damage_absorbtion(
                    damage, target)
            if damage_left < 1:
                text = '{0} має настільки високий захист, що навіть не помічає цю атаку'.format(  # noqa
                        target.name)
            if absorbed:
                text = '{0} має захист, який гасить {1} пошкоджень'.format(  # noqa
                        target.name, absorbed)
                log_action(quest, text, ActionLogType.ITEM.value)
            result = hit(target, damage_left, quest, character)
            # returns False if character flees
            if not result:
                return False
        else:
            text = '{0} ухиляється'.format(
                target.name)
            log_action(quest, text)
        return True
