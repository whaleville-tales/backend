from graphene import String, Boolean
from graphene.relay import ClientIDMutation
from graphql import GraphQLError
from whaleville.users.models import User
from whaleville.core.processors.generator import game_starter


class RegisterMutation(ClientIDMutation):
    response = Boolean()

    class Input:
        email = String()
        password1 = String()
        password2 = String()

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        required_fields = ('email', 'password1', 'password2')
        if not set(required_fields).issubset(input):
            raise GraphQLError(
                'Validation error. These fields are requred: {0}'.format(
                    ', '.join(required_fields)))
        if not input['password1'] == input['password2']:
            raise GraphQLError('Validation error. Passwords do not match')
        # create user
        user = User.objects.create(
            email=input['email'],
            is_active=True
        )
        user.set_password(input['password1'])
        user.save()
        # initiate game
        game_starter.start_game(user)
        return RegisterMutation(response=True)


class UserMutation:
    register = RegisterMutation.Field()
