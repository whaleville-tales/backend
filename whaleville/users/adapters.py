from typing import Any

from allauth.account.adapter import DefaultAccountAdapter
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter
from django.conf import settings
from django.http import HttpRequest


class AccountAdapter(DefaultAccountAdapter):

    def is_open_for_signup(self, request: HttpRequest):
        return getattr(settings, 'ACCOUNT_ALLOW_REGISTRATION', True)

    def send_mail(self, template_prefix, email, context):
        context['activate_url'] = settings.FRONTEND_URL + \
            '/account-confirm-email/?key=' + context['key']
        msg = self.render_mail(template_prefix, email, context)
        msg.send()


class SocialAccountAdapter(DefaultSocialAccountAdapter):

    def is_open_for_signup(self, request: HttpRequest, sociallogin: Any):
        return getattr(settings, 'ACCOUNT_ALLOW_REGISTRATION', True)
