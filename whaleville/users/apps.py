from django.apps import AppConfig


class UsersAppConfig(AppConfig):

    name = 'whaleville.users'
    verbose_name = 'Users'
