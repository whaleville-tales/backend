Install
=========

Prerequisites
-------------

* Python 3.6
* Pipenv
* Docker & Docker Compose

Installation
------------

1. Create local env file

::

    cp ./config/.env.template ./config/.env


2. Install pre-hook:

::

    pip install pre-commit

::

    pre-commit install


3. Run project backend

::

    make run


Notes
-----

* Create superuser:

    ::

        docker-compose run django python manage.py createsuperuser


* Project linting and tests:

    We are usign flake8 to lint project. Pytest to run tests and mypy for type checking.

    ::

        docker-compose run django flake8

    ::

        docker-compose run django pytest

    ::

        docker-compose run django mypy beaty_salon


* Documentation
    
    We use sphinx to document the project, so if you add something new please doc it here(according to scructure)

    We don't serve html files on services like readthedocs etc. So you need to generate them manually(see the last item in the next list), when you do it -- you'll be able to see the html-files ind ``docs/_build/html/imdex.html``, just open in your browser

    * install sphinx on your locall machine 

    ::

        pip install Sphinx

    * make changes
    * run in the terminal

    ::

        cd docs && make html


