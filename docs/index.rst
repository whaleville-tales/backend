.. Documentation master file, created by
   sphinx-quickstart.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to documentation!
====================================================================

Contents:

.. toctree::
   :maxdepth: 2

   install
   api
   deploy
   docker_ec2



Indices and tables
==================

* :ref:`search`
